$Page Sends And Returns APC80 Template
$TargetName Beat the Clock

APC80.tif

Welcome to the /Sends And Returns APC80 Template/ created by Gangsterish.

*Quick Start*

-> 0. Run the included installer
-> 1. Start Ableton
-> 2. Open Preferences
-> 3. Under MIDI:
->	For whichever control surface(s) you will be using, turn on ‘Track’ and ‘Remote’
->	 - Set APC 40 Control Surface drop down to APC80_40
->	 - Set APC 20 Control Surface drop down to APC80_20
-> 4. Under Record-Warp-Launch:
->	 - Turn 'Select on Launch' Off.
-> 6. Open APC80.als
->	 - If the right deck (APC20) is controlling the left side, Deck A, they connected in the wrong order. Open Preferences again, set APC 20 Control Surface to None, then back to APC80_20 again to reset it.
-> 7. Send the Audio Out in the I-O section of the Cue A and B tracks, to your cue audio channel, to enable auto cueing.
-> 8. Hit the 'Down' button on your APC, hit a yellow button to start playing a track!

*Intermediate Reference*

-> Delete the included tracks, bring in your own tracks and warp them.
-> $Link /Warping Guide/ <http://bit.ly/WarpingGuide>
-> Open the small gray group on the far left and set Audio From input to your Microphone channel. Make sure to close the gray group when you're done.
-> Open the Red 'Inputs' group and set 'Audio From' for any external inputs (CDJs, etc.) you may be using.
-> Add your own samples into the 'Sample' track under 'Noises'.
-> Add your own MIDI instruments to the other 'Noises' tracks.

*Advanced Reference*

-> To remove this lesson just delete (or rename) the 'Sends And Returns APC80 Template Lessons' folder inside the Project.

*Links*

-> $Link /Sends & Returns/ <http://sandr.co/>
-> $Link /APC80 Reference/ <http://apc80.com/>
-> $Link /Gangsterish/ <http://gangsterish.com>

SandR.tif

$Comment Status: No translation
