import Live
from APC80_40.GSessionComponent import GSessionComponent
class GAPCSessionComponent(GSessionComponent):
    " Special SessionComponent for the APC controllers' combination mode "

    def __init__(self, parent, num_tracks, num_scenes):
        self._parent = parent
        GSessionComponent.__init__(self, parent, num_tracks, num_scenes)

    def link_with_track_offset(self, track_offset):
        assert (track_offset >= 0)
        if self._is_linked():
            self._unlink()
        self.set_offsets(track_offset, 0)
        # self._link()

    def unlink(self):
        if self._is_linked():
            self._unlink()
