#Embedded file name: /Users/versonator/Hudson/live/Projects/AppLive/Resources/MIDI Remote Scripts/_Framework/SessionZoomingComponent.py
from _Framework.SubjectSlot import subject_slot
from _Framework.CompoundComponent import CompoundComponent
from _Framework.SessionZoomingComponent import *
from _Framework.ScrollComponent import ScrollComponent
from _Framework.Util import in_range
from consts import *

class GSessionZoomingComponent(CompoundComponent):
    """
    Class using a matrix of buttons to choose blocks of clips in the
    session, as if you had zoomed out from session.
    """

    def __init__(self, parent, track_offset, *a, **k):
        super(GSessionZoomingComponent, self).__init__(*a, **k)
        # assert isinstance(session, SessionComponent)
        self._buttons = None
        self._zoom_button = None
        self._scene_bank_buttons = None
        self._scene_bank_button_slots = self.register_slot_manager()
        self._scene_bank_index = 0
        self._is_zoomed_out = False
        self._empty_value = 0
        self._stopped_value = 100
        self._playing_value = 127
        self._selected_value = 64
        self._parent = parent
        self._track_offset = track_offset
        self._ignore_buttons = False
        self._session, self._vertical_scroll, self._horizontal_scroll = self.register_components(parent._session, ScrollComponent(), ScrollComponent())
        self._vertical_scroll.can_scroll_up = self._can_scroll_up
        self._vertical_scroll.can_scroll_down = self._can_scroll_down
        self._vertical_scroll.scroll_up = self._scroll_up
        self._vertical_scroll.scroll_down = self._scroll_down
        self._horizontal_scroll.can_scroll_up = self._can_scroll_left
        self._horizontal_scroll.can_scroll_down = self._can_scroll_right
        self._horizontal_scroll.scroll_up = self._scroll_left
        self._horizontal_scroll.scroll_down = self._scroll_right
        self.register_slot(self._session, self._on_session_offset_changes, 'offset')

    def on_scene_list_changed(self):
        self.update()

    def set_ignore_buttons(self, ignore):
        assert isinstance(ignore, type(False))
        if (self._ignore_buttons != ignore):
            self._ignore_buttons = ignore
            if (not self._is_zoomed_out):
                self._session.set_enabled((not ignore))
            self.update()

    def on_enabled_changed(self):
        super(GSessionZoomingComponent, self).on_enabled_changed()
        self._session.set_show_highlight(self.is_enabled())

    def set_button_matrix(self, buttons):
        if buttons != self._buttons:
            self._buttons = buttons
            self._on_matrix_value.subject = self._buttons
        self.update()

    def set_zoom_button(self, button):
        if button != self._zoom_button:
            self._zoom_button = button
            self._on_zoom_value.subject = button
            self._is_zoomed_out = False
            self.update()

    def set_nav_buttons(self, up, down, left, right):
        self.set_nav_up_button(up)
        self.set_nav_down_button(down)
        self.set_nav_left_button(left)
        self.set_nav_right_button(right)
        self.update()

    def set_nav_up_button(self, button):
        self._vertical_scroll.set_scroll_up_button(button)

    def set_nav_down_button(self, button):
        self._vertical_scroll.set_scroll_down_button(button)

    def set_nav_left_button(self, button):
        self._horizontal_scroll.set_scroll_up_button(button)

    def set_nav_right_button(self, button):
        self._horizontal_scroll.set_scroll_down_button(button)

    def set_scene_bank_buttons(self, buttons):
        if self._scene_bank_buttons != buttons:
            self._scene_bank_button_slots.disconnect()
            self._scene_bank_buttons = buttons
            if self._scene_bank_buttons != None:
                for button in self._scene_bank_buttons:
                    self._scene_bank_button_slots.register_slot(button, self._on_scene_bank_value, 'value', extra_kws=dict(identify_sender=True))

            self.update()

    def set_empty_value(self, value):
        value = int(value)
        assert (value in range(128))
        self._empty_value = value

    def set_playing_value(self, value):
        value = int(value)
        assert (value in range(128))
        self._playing_value = value

    def set_stopped_value(self, value):
        value = int(value)
        assert (value in range(128))
        self._stopped_value = value

    def set_selected_value(self, value):
        value = int(value)
        assert (value in range(128))
        self._selected_value = value

    def _session_set_enabled(self, is_enabled):
        """
        Override to customize behaviour when session is disabled.
        """
        self._session.set_enabled(is_enabled)

    def update(self):
        if self._allow_updates:
            self._session_set_enabled(not self._is_zoomed_out)
            if self.is_enabled():
                if self._is_zoomed_out and self._buttons != None:
                    tracks = self._session.tracks_to_use()
                    scenes = self.song().scenes
                    slots_registry = [ None for index in range(len(scenes)) ]
                    width = self._session.width()
                    height = self._session.height() + 1
                    current_bpm = float(self.song().tempo)
                    ## Gangsterish
                    for buttonX in range(self._buttons.width()):
                        for buttonY in range(self._buttons.height()):
                            value_to_send = self._empty_value
                            # If the row is the 3rd or 6th row, keep it dark.
                            if (buttonX != 2 and buttonX != 5):
                                # Zoomy - For each column, grab 'pages' further down
                                if (buttonX == 3):
                                    sceneY = buttonY + 5
                                    sceneX = 0
                                elif (buttonX == 4):
                                    sceneY = buttonY + 5
                                    sceneX = 1
                                elif (buttonX == 6):
                                    sceneY = buttonY + 10
                                    sceneX = 0
                                elif (buttonX == 7):
                                    sceneY = buttonY + 10
                                    sceneX = 1
                                else:
                                    sceneY = buttonY
                                    sceneX = buttonX
                                scene_bank_offset = ((self._scene_bank_index * self._buttons.height()) * height)
                                track_offset = (sceneX * width) + 4
                                scene_offset = ((sceneY * height) + scene_bank_offset)
                                # Gangsterish
                                # This colors the buttons according to zoom scroll depth
                                # So as you scroll, the buttons turn from yellow, to red, to green
                                # 0=off, 1=green, 2=green blink, 3=red, 4=red blink, 5=yellow, 6=yellow blink, 7-127=green
                                if ((track_offset in range(len(tracks))) and (scene_offset in range(len(scenes)))):
                                    if (sceneX == 1 and buttonY < (scene_offset/30)-10):
                                        value_to_send = COLOR_RED
                                    elif (sceneX == 1 and buttonY < (scene_offset/30)-5):
                                        value_to_send = COLOR_RED_BLINK
                                    elif (sceneX == 1 and buttonY < (scene_offset/30)):
                                        value_to_send = COLOR_YELLOW
                                    else:
                                        value_to_send = COLOR_RED
                                    # self._parent.log_message('            self._session.track_offset(): ' + str(self._session.track_offset()))
                                    # self._parent.log_message('            session.scene_offset: ' + str(self._session.scene_offset()))
                                    # self._parent.log_message('             buttonY: ',str(buttonY),' buttonX: ',str(buttonX))
                                    # Show our current location with Yellow Blink
                                    if (
                                        (self._session.track_offset()-self._track_offset == buttonX)
                                        # Divide the scene offset by the scroll height, mod it by the view pane height
                                        and ((self._session.scene_offset() / 6) % 5 == buttonY)
                                    ):
                                        value_to_send = COLOR_YELLOW_BLINK
                                    else:
                                        playing = False
                                        for track in range(track_offset, track_offset + width):
                                            for scene in range(scene_offset, scene_offset + height):
                                                if track in range(len(tracks)) and scene in range(len(scenes)):
                                                    if slots_registry[scene] == None:
                                                        slots_registry[scene] = scenes[scene].clip_slots
                                                    slot = slots_registry[scene][track] if len(slots_registry[scene]) > track else None
                                                    if slot != None and slot.has_clip:
                                                        if slot.clip.is_playing:
                                                            value_to_send = COLOR_GREEN_BLINK
                                                            playing = True
                                                            break
                                                        try:
                                                            bpm = slot.clip.name.split(' ')[0]
                                                            if float(bpm) == current_bpm:
                                                                value_to_send = COLOR_GREEN
                                                        except:
                                                            continue
                                            if playing:
                                                break

                            self._buttons.send_value(buttonX, buttonY, value_to_send)

                if self._scene_bank_buttons != None:
                    for index in range(len(self._scene_bank_buttons)):
                        if self._is_zoomed_out and index == self._scene_bank_index:
                            self._scene_bank_buttons[index].turn_on()
                        else:
                            self._scene_bank_buttons[index].turn_off()

        else:
            self._update_requests += 1

    def _on_session_offset_changes(self):
        if self._is_zoomed_out:
            self._scene_bank_index = int(self._session.scene_offset() / self._session.height() / self._buttons.height())
        self.update()

    @subject_slot('value')
    def _on_zoom_value(self, value):
        if self.is_enabled():
            if self._zoom_button.is_momentary():
                self._is_zoomed_out = value > 0
            else:
                self._is_zoomed_out = not self._is_zoomed_out
            if self._is_zoomed_out and self._buttons:
                self._scene_bank_index = int(self._session.scene_offset() / self._session.height() / self._buttons.height())
            else:
                self._scene_bank_index = 0
            self._session_set_enabled(not self._is_zoomed_out)
            self.update()

    @subject_slot('value')
    def _on_matrix_value(self, value, buttonX, buttonY, is_momentary):
        if self.is_enabled() and self._is_zoomed_out:
            if ((value != 0) or (not is_momentary)):
                ## Gangsterish
                if (buttonX == 2 or buttonX == 5):
                    value_to_send = self._empty_value
                else:
                    if (buttonX == 1):
                        sceneY = buttonY
                    elif (buttonX == 3 or buttonX == 4):
                        sceneY = buttonY + 5
                    elif (buttonX == 6 or buttonX == 7):
                        sceneY = buttonY + 10
                    else:
                        sceneY = buttonY
                track_offset = self._session.track_offset()
                scene_offset = ((sceneY + (self._scene_bank_index * self._buttons.height())) * (self._session.height()+1))
                if ((track_offset in range(len(self._session.tracks_to_use()))) and (scene_offset in range(len(self.song().scenes)))):
                    self._session.set_offsets(track_offset, scene_offset)

    def _on_scene_bank_value(self, value, sender):
        if self.is_enabled() and self._is_zoomed_out:
            if value != 0 or not sender.is_momentary():
                button_offset = list(self._scene_bank_buttons).index(sender)
                scene_offset = button_offset * self._buttons.height() * self._session.height()
                if scene_offset in range(len(self.song().scenes)):
                    self._scene_bank_index = button_offset
                    self.update()

    def _can_scroll_up(self):
        return self._session._can_bank_up()

    def _can_scroll_down(self):
        return self._session._can_bank_down()

    def _can_scroll_left(self):
        return self._session._can_bank_left()

    def _can_scroll_right(self):
        return self._session._can_bank_right()

    def _scroll_up(self):
        if self._is_zoomed_out:
            # Gangsterish
            # Compensate for page separator scenes.
            height = self._session.height() + 1
            track_offset = self._session.track_offset()
            scene_offset = self._session.scene_offset()
            if scene_offset > 0:
                new_scene_offset = scene_offset
                if ((scene_offset % height) > 0):
                    new_scene_offset -= (scene_offset % height)
                else:
                    if (self._is_zoomed_out):
                        # Scroll by much larger increment
                        new_scene_offset = max(0, (scene_offset - (height * 5)))
                    else:
                        new_scene_offset = max(0, (scene_offset - height))
                self._session.set_offsets(track_offset, new_scene_offset)

    def _scroll_down(self):
        if self._is_zoomed_out:
            # Compensate for page separator scenes.
            height = self._session.height() + 1
            track_offset = self._session.track_offset()
            scene_offset = self._session.scene_offset()
            # Gangsterish
            # Scroll by much larger increment
            new_scene_offset = scene_offset + (height * 5) - scene_offset % height
            self._session.set_offsets(track_offset, new_scene_offset)

    def _scroll_left(self):
        if self._is_zoomed_out:
            width = self._session.width()
            track_offset = self._session.track_offset()
            scene_offset = self._session.scene_offset()
            if track_offset > 0:
                new_track_offset = track_offset
                if track_offset % width > 0:
                    new_track_offset -= track_offset % width
                else:
                    new_track_offset = max(0, track_offset - width)
                self._session.set_offsets(new_track_offset, scene_offset)

    def _scroll_right(self):
        if self._is_zoomed_out:
            width = self._session.width()
            track_offset = self._session.track_offset()
            scene_offset = self._session.scene_offset()
            new_track_offset = track_offset + width - track_offset % width
            self._session.set_offsets(new_track_offset, scene_offset)