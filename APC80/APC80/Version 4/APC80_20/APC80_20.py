import Live
import sys
from APC80_40.APC import APC
from _Framework.InputControlElement import *
from _Framework.SliderElement import SliderElement
from _Framework.ButtonElement import ButtonElement
from _Framework.EncoderElement import EncoderElement
from _Framework.ButtonMatrixElement import ButtonMatrixElement
from _Framework.TransportComponent import TransportComponent
from _Framework.SessionZoomingComponent import *
from APC80_40.SpecialMixerComponent import SpecialMixerComponent
from SliderModesComponent import SliderModesComponent
from consts import *

# Gangsterish
from GAPCSessionComponent import GAPCSessionComponent
from VUMeters import VUMeters
from APC80_40.SimpleVU import SimpleVU
from APC80_40.GSessionZoomingComponent import GSessionZoomingComponent
from APC80_40.GMomentaryButtonElement import GMomentaryButtonElement
from APC80_40.GButtonMatrixElement import GButtonMatrixElement
from APC80_40.GButtonElement import GButtonElement
from APC80_40.MatrixModesComponent import MatrixModesComponent

FOLDER = '/APC80_40/'

class APC80_20(APC):
    """ Script for Akai's APC20 Controller """

    def __init__(self, c_instance):
        self._shift_modes = None
        APC.__init__(self, c_instance)

        # Gangsterish
        self.song().add_current_song_time_listener(self.on_time_changed)
        self.song().add_is_playing_listener(self.on_time_changed)
        self._current_bar = 1
        self._current_beat = 1
        self._current_quarter_beat = 1
        self._current_eight_beat = 1
        self._metronome = 1
        self._mic_flash = 0
        self._has_cleared_fx = 0
        self._slower = False
        self._faster = False

    def disconnect(self):
        self._shift_modes = None
        self.song().remove_current_song_time_listener(self.on_time_changed) 
        self.song().remove_is_playing_listener(self.on_time_changed)
        APC.disconnect(self)

    def _activate_combination_mode(self, track_offset, support_devices):
        APC._activate_combination_mode(self, track_offset, support_devices)
        # if support_devices:
        #     self._shift_modes.invert_assignment()

    def _setup_session_control(self):
        self.get_user_settings()
        is_momentary = True
        self._shift_button = ButtonElement(is_momentary, MIDI_NOTE_TYPE, 0, 81)
        self.log_message("WIDTH:",str(self._user_variables['APC20_WIDTH']))
        self.log_message("HEIGHT:",str(self._user_variables['APC20_HEIGHT']))
        self._session = GAPCSessionComponent(self, int(self._user_variables['APC20_WIDTH']), int(self._user_variables['APC20_HEIGHT']))
        self._session.name = 'Session_Control'
        self._matrix = GButtonMatrixElement()
        self._matrix.name = 'Button_Matrix'
        scene_launch_buttons = [ ButtonElement(is_momentary, MIDI_NOTE_TYPE, 0, index + 82) for index in range(5) ]
        track_stop_buttons = [ ButtonElement(is_momentary, MIDI_NOTE_TYPE, index, 52) for index in range(8) ]
        self._mute_buttons = []
        self._solo_buttons = []
        self._arm_buttons = []
        for index in range(len(scene_launch_buttons)):
            scene_launch_buttons[index].name = 'Scene_' + str(index) + '_Launch_Button'

        for index in range(len(track_stop_buttons)):
            track_stop_buttons[index].name = 'Track_' + str(index) + '_Stop_Button'
        self._stop_buttons = track_stop_buttons

        self._session.set_stop_track_clip_buttons(tuple(track_stop_buttons))
        self._session.set_stop_track_clip_value(2)
        for scene_index in range(int(self._user_variables['APC20_HEIGHT'])):
            scene = self._session.scene(scene_index)
            scene.name = 'Scene_' + str(scene_index)
            button_row = []
            scene.set_launch_button(scene_launch_buttons[scene_index])
            scene.set_triggered_value(2)
            for track_index in range(int(self._user_variables['APC20_WIDTH'])):
                clip_slot = scene.clip_slot(track_index)
                button = GButtonElement(is_momentary, MIDI_NOTE_TYPE, track_index, (scene_index + 53))
                button.name = str(track_index) + '_Clip_' + str(scene_index) + '_Button'
                button_row.append(button)
                clip_slot.name = str(track_index) + '_Clip_Slot_' + str(scene_index)
                clip_slot.set_triggered_to_play_value(2)
                clip_slot.set_triggered_to_record_value(4)
                clip_slot.set_recording_value(3)
                clip_slot.set_launch_button(button)
                clip_slot = scene.clip_slot(track_index)
                clip_slot.set_stopped_value(5)
                clip_slot.set_started_value(1)
            self._matrix.add_row(tuple(button_row))
            # self._button_rows.append(button_row)
        self._session.selected_scene().name = 'Selected_Scene'
        self._session.selected_scene().set_launch_button(ButtonElement(is_momentary, MIDI_CC_TYPE, 0, 64))
        # self._session_zoom = DeprecatedSessionZoomingComponent(self, self._session)
        # self._session_zoom.name = 'Session_Overview'
        # self._session_zoom.set_button_matrix(self._matrix)
        # self._session_zoom.set_zoom_button(self._shift_button)
        # self._session_zoom.set_scene_bank_buttons(tuple(scene_launch_buttons))
        # self._session_zoom.set_stopped_value(3)
        # self._session_zoom.set_selected_value(5)

    def on_time_changed(self):
        bar = int(str(self.song().get_current_beats_song_time()).split('.')[0])
        beat = int(str(self.song().get_current_beats_song_time()).split('.')[1])
        quarter_beat = int(str(self.song().get_current_beats_song_time()).split('.')[2])
        eight_beat = int(str(self.song().get_current_beats_song_time()).split('.')[3])
        if bar != self._current_bar:
            self._slower = False
            self._faster = False
            self._current_bar = bar
            foundBPM = False
            for scene_index in range(int(self._user_variables['APC40_HEIGHT'])):
                scene = self._session.scene(scene_index)
                for track_index in range(int(self._user_variables['APC40_WIDTH'])):
                    clip_slot = scene.clip_slot(track_index)
                    if (clip_slot._clip_slot and clip_slot._clip_slot.clip):
                        bpm = clip_slot._clip_slot.clip.name.split(' ')[0]
                        try:
                            track_bpm = float("%.2f" % float(bpm))
                        except:
                            break
                        foundBPM = True
                        break
                if foundBPM:
                    break
            if foundBPM:
                self._APC40 = APC._active_instances[0]
                # self.log_message("APC40: "+self._APC40)

                song_bpm = self.song().tempo
                if self._APC40._tempo_ramp_active:
                    song_bpm = self._APC40._tempo_ramp_settings[0]
                if track_bpm < song_bpm:
                    self._faster = True
                if track_bpm > song_bpm:
                    self._slower = True
        if beat != self._current_beat:
            # self.log_message("Current beat: "+str(beat))
            # self.log_message("Current bar: "+str(bar))
            beatOfEight = beat
            if (bar % 2):
                beatOfEight += 4
            self._current_beat = beat
            self._ofEight = ofEight = beatOfEight - 1 # Turn 1-8 into 0-7
            self._solo_buttons[ofEight].send_value(127, True)
            self._solo_buttons[ofEight - 1].send_value(0, True)

            if self._slower == True:
                # self.log_message("Slower! ofEight: "+str((ofEight + 1) * -1))
                self._stop_buttons[ofEight * -1].send_value(127, True)
                nextBtn = (ofEight - 1) * -1
                self._stop_buttons[nextBtn].send_value(0, True)
            elif self._faster == False:
                for index in range(0,8):
                    self._stop_buttons[index].send_value(0, True)

            if self._has_cleared_fx == 0:
                self._has_cleared_fx = 1
                for btn in self._mute_buttons:
                    btn.send_value(0, True)
                for btn in self._solo_buttons:
                    btn.send_value(0, True)
                for btn in self._arm_buttons:
                    btn.send_value(0, True)

        if quarter_beat != self._current_quarter_beat:
            self._current_quarter_beat = quarter_beat
            if self.song().tracks[3].mute == False:
                if self._mic_flash == 1:
                    for btn in self._mute_buttons:
                        btn.send_value(127, True)
                    for btn in self._solo_buttons:
                        btn.send_value(127, True)
                    for btn in self._arm_buttons:
                        btn.send_value(127, True)
                    self._mic_flash = 0
                else:
                    for btn in self._mute_buttons:
                        btn.send_value(0, True)
                    for btn in self._solo_buttons:
                        btn.send_value(0, True)
                    for btn in self._arm_buttons:
                        btn.send_value(0, True)
                    self._mic_flash = 1

            # self.log_message("Current ebeat: "+str(eight_beat))
            # self.log_message("Current qbeat: "+str(quarter_beat))
            ebeatOfEight = quarter_beat -1
            if (beat % 2):
                ebeatOfEight += 4
            if self._faster == True:
                # self.log_message("Faster! ofEight: "+str(ebeatOfEight))
                self._stop_buttons[ebeatOfEight].send_value(127, True)
                prevBtn = (ebeatOfEight - 1)
                self._stop_buttons[prevBtn].send_value(0, True)

        if eight_beat != self._current_eight_beat:
            self._current_eight_beat = eight_beat
            self._session.recolor_buttons();

    def _setup_mixer_control(self):
        is_momentary = True
        self._mixer = SpecialMixerComponent(8)
        self._mixer.name = 'Mixer'
        self._mixer.master_strip().name = 'Master_Channel_Strip'
        self._mixer.selected_strip().name = 'Selected_Channel_Strip'
        self._mute_buttons = []
        for track in range(8):
            strip = self._mixer.channel_strip(track)
            strip.name = 'Channel_Strip_' + str(track)
            arm_button = ButtonElement(is_momentary, MIDI_NOTE_TYPE, track, 48)
            solo_button = GMomentaryButtonElement(self, MIDI_NOTE_TYPE, track, 49)
            mute_button = GMomentaryButtonElement(self, MIDI_NOTE_TYPE, track, 50)
            mute_button.send_value(0, True)
            solo_button.name = str(track) + '_Solo_Button'
            mute_button.name = str(track) + '_Mute_Button'
            strip.set_solo_button(solo_button)
            strip.set_mute_button(mute_button)
            strip.set_arm_button(arm_button)
            strip.set_shift_button(self._shift_button)
            strip.set_invert_mute_feedback(True)
            self._mute_buttons.append(mute_button)
            self._solo_buttons.append(solo_button)


        master_volume_control = SliderElement(MIDI_CC_TYPE, 0, 14)
        prehear_control = EncoderElement(MIDI_CC_TYPE, 0, 48, Live.MidiMap.MapMode.relative_two_compliment)
        master_volume_control.name = 'Master_Volume_Control'
        prehear_control.name = 'Prehear_Volume_Control'
        self._mixer.set_prehear_volume_control(prehear_control)
        self._mixer.master_strip().set_volume_control(master_volume_control)

    def _setup_custom_components(self):
        is_momentary = True
        master_select_button = ButtonElement(is_momentary, MIDI_NOTE_TYPE, 0, 80)
        master_select_button.name = 'Master_Select_Button'
        select_buttons = []
        arm_buttons = []
        sliders = []
        all_select_buttons = []
        for track in range(8):
            select_button = GButtonElement(is_momentary, MIDI_NOTE_TYPE, track, 51)
            select_buttons.append(select_button)
            all_select_buttons.append(select_button)
            arm_buttons.append(ButtonElement(is_momentary, MIDI_NOTE_TYPE, track, 48))
            sliders.append(SliderElement(MIDI_CC_TYPE, track, 7))
            select_buttons[-1].name = str(track) + '_Select_Button'
            arm_buttons[-1].name = str(track) + '_Arm_Button'
            sliders[-1].name = str(track) + '_Volume_Control'

        transport = TransportComponent()
        transport.name = 'Transport'
        slider_modes = SliderModesComponent(self._mixer, tuple(sliders))
        slider_modes.name = 'Slider_Modes'

        self._arm_buttons = arm_buttons

        # Gangsterish
        vu = VUMeters(self)
        vu0 = SimpleVU(self,12,12)
        vu1 = SimpleVU(self,13,12)
        vu2 = SimpleVU(self,14,12)
        vu3 = SimpleVU(self,15,12)
        vu4 = SimpleVU(self,16,12)
        vu5 = SimpleVU(self,17,12)
        vu6 = SimpleVU(self,18,12)
        vu7 = SimpleVU(self,19,12)

        matrix_modes = MatrixModesComponent(self._matrix, self._session, self._session_zoom, self)
        matrix_modes.name = 'Matrix_Modes'
        self._matrix_modes = matrix_modes
        all_select_buttons.append(master_select_button)
        matrix_modes.set_mode_buttons(all_select_buttons)

        self._session.recolor_buttons()

    def _product_model_id_byte(self):
        return 123

    def get_user_settings(self):
        if not hasattr(self, "_user_variables"):
            self._user_variables = {}
        script_path = ''
        for path in sys.path:
            if 'MIDI Remote Scripts' in path:
                script_path = path
                break
        user_file = script_path + FOLDER + 'Settings.txt'
        for line in open(user_file): 
            line = line.rstrip('\n')
            if not line.startswith(('#', '"',)) and not line == '' and '=' in line:
                var_data = line.split('=')
                if len(var_data) >= 2 and not ';' in var_data[1] and not '%' in var_data[1] and not '=' in var_data[1]:
                    self._user_variables[var_data[0].strip()] = var_data[1].strip()
