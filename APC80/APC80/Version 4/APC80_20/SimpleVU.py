import Live
from _Framework.ControlSurfaceComponent import ControlSurfaceComponent
from _Framework.ButtonElement import ButtonElement
from _Framework.SessionComponent import SessionComponent
import math

# Velocity values for clip colours. Different on some devices
LED_RED = 3
LED_ON = 127
LED_OFF = 0
LED_ORANGE = 5

# Scaling constants. Narrows the db range we display to 0db-21db or thereabouts
CHANNEL_SCALE_MAX = 0.92
CHANNEL_SCALE_MIN = 0.52
CHANNEL_SCALE_INCREMENTS = 10

RMS_FRAMES = 2
USE_RMS = True

class VUMeter():
  'represents a single VU to store RMS values etc in'
  def __init__(self, parent, track, top, bottom,
              increments, channel):

    self.frames = [0.0] * RMS_FRAMES
    self.parent = parent
    self.track = track
    self.top = top
    self.bottom = bottom
    self.multiplier = self.calculate_multiplier(top, bottom, increments)
    self.current_level = 0
    self.channel = channel
    self.matrix = self.setup_matrix()

  def observe(self):
    new_frame = self.mean_peak()
    self.store_frame(new_frame)

    if not self.parent._clipping:
      if USE_RMS:
        level = self.scale(self.rms(self.frames))
      else:
        level = self.scale(new_frame)
      if level != self.current_level:
        self.current_level = level
        self.parent.set_leds(self.matrix, level)

  def store_frame(self, frame):
    self.frames.pop(0)
    self.frames.append(frame)

  def rms(self, frames):
    return math.sqrt(sum(frame*frame for frame in frames)/len(frames))

  # return the mean of the L and R peak values
  def mean_peak(self):
    return (self.track.output_meter_left + self.track.output_meter_right) / 2


  # Perform the scaling as per params. We reduce the range, then round it out to integers
  def scale(self, value):
    if (value > self.top):
      value = self.top
    elif (value < self.bottom):
      value = self.bottom
    value = value - self.bottom
    value = value * self.multiplier #float, scale 0-10
    return int(round(value))

  def calculate_multiplier(self, top, bottom, increments):
    return (increments / (top - bottom))


  def setup_matrix(self):
    matrix = []
    # 8 tracks, 4 offsets = 12. Shift 12 to get back to 0.
    strip = self.parent._parent._mixer.channel_strip(self.channel-12)
    matrix.append(strip._arm_button)
    return matrix


class SimpleVU(ControlSurfaceComponent):
    'standalone class used to handle VU meters'

    def __init__(self, parent, channel = False):
        # Boilerplate
        ControlSurfaceComponent.__init__(self)
        self._parent = parent

        # Default the audio levels to 0
        self._meter_level = 0
        self._audio_level = 0

        # We don't start clipping
        self._clipping = False

        # The track we'll be pulling RMS from
        self._audio_track = self.song().tracks[channel]
        # self._parent.log_message("Initing VU on Channel: "+str(channel))
        #setup classes
        self.audio_meter = VUMeter(self, self._audio_track,
                                  CHANNEL_SCALE_MAX,
                                  CHANNEL_SCALE_MIN, CHANNEL_SCALE_INCREMENTS, channel)
        # Listeners!
        self._audio_track.add_output_meter_left_listener(self.audio_meter.observe)

    # If you fail to kill the listeners on shutdown, Ableton stores them in memory and punches you in the face
    def disconnect(self):
        self._audio_track.remove_output_meter_left_listener(self.audio_meter.observe)

    # Iterate through every column in the matrix, light up the LEDs based on the level
    # Level for channels is scaled to 10 cos we have 10 LEDs
    # Top two LEDs are red, the next is orange
    def set_leds(self, matrix, level):
          if level > 1:
            # self._parent.log_message("Turn on: "+str(matrix[0]))
            matrix[0].send_value(LED_ON, True)
          else:
            matrix[0].send_value(LED_OFF, True)

    # boilerplate
    def update(self):
        pass

    def on_enabled_changed(self):
        self.update()

    def on_selected_track_changed(self):
        self.update()

    def on_track_list_changed(self):
        self.update()

    def on_selected_scene_changed(self):
        self.update()

    def on_scene_list_changed(self):
        self.update()