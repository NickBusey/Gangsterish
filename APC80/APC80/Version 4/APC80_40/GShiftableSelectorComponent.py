# http://remotescripts.blogspot.com
"""
Customized APC40 control surface script
Copyright (C) 2010 Hanz Petrov <hanz.petrov@gmail.com>
Additional modification for Ableton Live 9 - Fabrizio Poce 2013 - <http://www.fabriziopoce.com/>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import Live
from _Framework.ModeSelectorComponent import ModeSelectorComponent
from _Framework.ButtonElement import ButtonElement
from _Framework.DeviceComponent import DeviceComponent 
from _Framework.SessionZoomingComponent import SessionZoomingComponent #added

class GShiftableSelectorComponent(ModeSelectorComponent):
    """ SelectorComponent that assigns buttons to functions based on the shift button """

    def __init__(self, parent, select_buttons, stop_buttons, matrix, session, zooming, mixer, matrix_modes, bpm_control=None):
        assert len(select_buttons) == 9
        ModeSelectorComponent.__init__(self)
        self._toggle_pressed = False
        self._note_mode_active = False
        self._invert_assignment = False
        self._select_buttons = select_buttons
        self._matrix_modes = matrix_modes
        self._stop_buttons = stop_buttons
        self._session = session
        self._zooming = zooming
        self._matrix = matrix
        self._mixer = mixer
        self._parent = parent
        self._bpm_control = bpm_control

    def disconnect(self):
        ModeSelectorComponent.disconnect(self)
        # self._master_button.remove_value_listener(self._master_value)
        self._select_buttons = None
        self._master_button = None
        self._slider_modes = None
        self._matrix_modes = None #added
        self._stop_buttons = None
        self._stop_all_button = None
        self._mute_buttons = None
        self._solo_buttons = None
        self._arm_buttons = None
        #self._transport = None
        self._session = None
        self._zooming = None
        self._matrix = None
        self._mixer = None
        self._parent = None #added
        # self._mode_callback = None

    def set_mode_toggle(self, button):
        ModeSelectorComponent.set_mode_toggle(self, button)
        self.set_mode(0)

    def invert_assignment(self):
        self._invert_assignment = True
        self._recalculate_mode()

    def number_of_modes(self):
        return 2

    def update(self):
        if self.is_enabled():
            if self._mode_index == 0: #shift released 
                self._bpm_control.set_enabled(True)
                self._matrix_modes.set_mode_buttons(None)
                bpm_control = self._parent._bpm_control
                for index in range(len(bpm_control._modes_buttons)):
                    if (index == bpm_control._mode_index):
                        bpm_control._modes_buttons[index].turn_on()
                    else:
                        bpm_control._modes_buttons[index].turn_off()
            else: #if shift pressed
                self._bpm_control.set_enabled(False)
                self._matrix_modes.set_mode_buttons(self._select_buttons)

    def _partial_refresh(self, value):
        for component in self._parent.components: #this is needed to force a refresh when manual mappings exist
            if isinstance(component, SessionZoomingComponent):
                component.update()

    def _toggle_value(self, value):
        assert self._mode_toggle != None
        assert value in range(128)
        self._toggle_pressed = value > 0
        self._recalculate_mode()
        if value < 1 and self._matrix_modes._last_mode > 1: #refresh on Shift button release, and if previous mode was Note Mode
            self._parent.schedule_message(2, self._partial_refresh, value)

    def _recalculate_mode(self):
        self.set_mode((int(self._toggle_pressed) + int(self._invert_assignment)) % self.number_of_modes())
