import Live
import sys
from APC import APC
from _Framework.ControlSurface import ControlSurface
from _Framework.InputControlElement import *
from _Framework.SliderElement import SliderElement
from _Framework.EncoderElement import EncoderElement
from _Framework.ButtonMatrixElement import ButtonMatrixElement
from _Framework.ButtonElement import ButtonElement
from _Framework.MixerComponent import MixerComponent
# from _Framework.SessionZoomingComponent import SessionZoomingComponent
from _Framework.ChannelTranslationSelector import ChannelTranslationSelector
from EncModeSelectorComponent import EncModeSelectorComponent
from RingedEncoderElement import RingedEncoderElement
from DetailViewCntrlComponent import DetailViewCntrlComponent
from ShiftableDeviceComponent import ShiftableDeviceComponent
from ShiftableTransportComponent import ShiftableTransportComponent
from ShiftTranslatorComponent import ShiftTranslatorComponent
from SpecialMixerComponent import SpecialMixerComponent
from PedaledSessionComponent import PedaledSessionComponent
from _Framework.ChannelStripComponent import ChannelStripComponent

#Gangsterish
from GButtonElement import GButtonElement
from GSessionZoomingComponent import GSessionZoomingComponent
from GAPCSessionComponent import GAPCSessionComponent
from GButtonMatrixElement import GButtonMatrixElement
from GClipSlotComponent import GClipSlotComponent
from GMomentaryButtonElement import GMomentaryButtonElement
from GSceneComponent import GSceneComponent
from VUMeters import VUMeters
from SimpleVU import SimpleVU
from GBPMComponent import GBPMComponent
from MatrixModesComponent import MatrixModesComponent
from consts import *
from GShiftableSelectorComponent import GShiftableSelectorComponent

FOLDER = '/APC80_40/'

class APC80_40(APC):
    """ Script for Akai's APC40 Controller """

    def __init__(self, c_instance):
        APC.__init__(self, c_instance)
        self._device_selection_follows_track_selection = True

        # Gangsterish
        self.song().add_current_song_time_listener(self.on_time_changed)
        self.song().add_is_playing_listener(self.on_time_changed)
        self._current_bar = 1
        self._current_beat = 1
        self._current_quarter_beat = 1
        self._current_eight_beat = 1
        self._metronome = 1
        self._last_beat = -1
        self._tempo = -1
        self._tempo_ramp_active = False
        self._tempo_ramp_settings = []
        self._has_cleared_fx = -1
        self._mic_flash = 0
        self._slower = False
        self._faster = False

    def disconnect(self):
        self._do_uncombine()
        self._shift_button = None
        self._matrix = None
        self._session = None
        self._session_zoom = None
        self._mixer = None
        self.song().remove_current_song_time_listener(self.on_time_changed) 
        self.song().remove_is_playing_listener(self.on_time_changed)
        ControlSurface.disconnect(self)

    def _setup_session_control(self):
        # Gangsterish
        self.get_user_settings()
        is_momentary = True
        self._shift_button = ButtonElement(is_momentary, MIDI_NOTE_TYPE, 0, 98)
        right_button = ButtonElement(is_momentary, MIDI_NOTE_TYPE, 0, 96)
        left_button = ButtonElement(is_momentary, MIDI_NOTE_TYPE, 0, 97)
        up_button = ButtonElement(is_momentary, MIDI_NOTE_TYPE, 0, 94)
        down_button = ButtonElement(is_momentary, MIDI_NOTE_TYPE, 0, 95)
        right_button.name = 'Bank_Select_Right_Button'
        left_button.name = 'Bank_Select_Left_Button'
        up_button.name = 'Bank_Select_Up_Button'
        down_button.name = 'Bank_Select_Down_Button'
        matrix = GButtonMatrixElement()
        matrix.name = 'Button_Matrix'
        self._session =  GAPCSessionComponent(self, int(self._user_variables['APC40_WIDTH']), int(self._user_variables['APC40_HEIGHT']))
        self._session.name = 'Session_Control'
        self._session.set_track_bank_buttons(right_button, left_button)
        self._session.set_scene_bank_buttons(down_button, up_button)
        scene_launch_buttons = [ ButtonElement(is_momentary, MIDI_NOTE_TYPE, 0, index + 82) for index in range(5) ]
        track_stop_buttons = [ GButtonElement(is_momentary, MIDI_NOTE_TYPE, index, 52) for index in range(8) ]

        # Gangsterish
        self._mute_buttons = []
        self._solo_buttons = []
        self._arm_buttons = []
        self._select_buttons = []
        self._stop_buttons = track_stop_buttons

        for index in range(len(scene_launch_buttons)):
            scene_launch_buttons[index].name = 'Scene_' + str(index) + '_Launch_Button'

        for index in range(len(track_stop_buttons)):
            track_stop_buttons[index].name = 'Track_' + str(index) + '_Stop_Button'

        # stop_all_button = ButtonElement(is_momentary, MIDI_NOTE_TYPE, 0, 81)
        # stop_all_button.name = 'Stop_All_Clips_Button'
        # self._session.set_stop_all_clips_button(stop_all_button)
        self._session.set_stop_track_clip_buttons(tuple(track_stop_buttons))
        self._session.set_stop_track_clip_value(2)

        self._button_rows = []
        for scene_index in range(int(self._user_variables['APC40_HEIGHT'])):
            scene = self._session.scene(scene_index)
            scene.name = 'Scene_' + str(scene_index)
            button_row = []
            scene.set_launch_button(scene_launch_buttons[scene_index])
            scene.set_triggered_value(2)
            for track_index in range(int(self._user_variables['APC40_WIDTH'])):
                clip_slot = scene.clip_slot(track_index)
                button = GButtonElement(is_momentary, MIDI_NOTE_TYPE, track_index, (scene_index + 53))
                button.name = str(track_index) + '_Clip_' + str(scene_index) + '_Button'
                button_row.append(button)
                clip_slot.name = str(track_index) + '_Clip_Slot_' + str(scene_index)
                clip_slot.set_triggered_to_play_value(COLOR_GREEN_BLINK)
                clip_slot.set_triggered_to_record_value(COLOR_RED_BLINK)
                clip_slot.set_recording_value(COLOR_RED)
                clip_slot.set_launch_button(button)
                clip_slot.set_stopped_value(COLOR_YELLOW)
                clip_slot.set_started_value(COLOR_GREEN)
            matrix.add_row(tuple(button_row))
            self._button_rows.append(button_row)

        # self._session.set_slot_launch_button(ButtonElement(is_momentary, MIDI_CC_TYPE, 0, 67))
        self._session.selected_scene().name = 'Selected_Scene'
        self._session.selected_scene().set_launch_button(ButtonElement(is_momentary, MIDI_CC_TYPE, 0, 64))
        self._session_zoom = GSessionZoomingComponent(self, 1)
        self._session_zoom.name = 'Session_Overview'
        self._session_zoom.set_button_matrix(matrix)
        self._session_zoom.set_zoom_button(self._shift_button)
        self._session_zoom.set_nav_buttons(up_button, down_button, left_button, right_button)
        self._session_zoom.set_scene_bank_buttons(tuple(scene_launch_buttons))
        self._session_zoom.set_stopped_value(3)
        self._session_zoom.set_selected_value(5)
        self._matrix = matrix


    def on_time_changed(self):
        bar = int(str(self.song().get_current_beats_song_time()).split('.')[0])
        beat = int(str(self.song().get_current_beats_song_time()).split('.')[1])
        quarter_beat = int(str(self.song().get_current_beats_song_time()).split('.')[2])
        eight_beat = int(str(self.song().get_current_beats_song_time()).split('.')[3])
        if bar != self._current_bar:
            self._slower = False
            self._faster = False
            self._current_bar = bar
            foundBPM = False
            for scene_index in range(int(self._user_variables['APC40_HEIGHT'])):
                scene = self._session.scene(scene_index)
                for track_index in range(int(self._user_variables['APC40_WIDTH'])):
                    clip_slot = scene.clip_slot(track_index)
                    if (clip_slot._clip_slot and clip_slot._clip_slot.clip):
                        bpm = clip_slot._clip_slot.clip.name.split(' ')[0]
                        try:
                            track_bpm = float("%.2f" % float(bpm))
                        except:
                            break
                        foundBPM = True
                        break
                if foundBPM:
                    break
            if foundBPM:
                song_bpm = self.song().tempo
                if self._tempo_ramp_active:
                    song_bpm = self._tempo_ramp_settings[0]
                if track_bpm < song_bpm:
                    self._faster = True
                if track_bpm > song_bpm:
                    self._slower = True
        if beat != self._current_beat:
            # self.log_message("Current beat: "+str(beat))
            # self.log_message("Current bar: "+str(bar))
            beatOfEight = beat
            if (bar % 2):
                beatOfEight += 4
            self._current_beat = beat
            self._ofEight = ofEight = beatOfEight - 1 # Turn 1-8 into 0-7
            self._solo_buttons[ofEight].send_value(127, True)
            self._solo_buttons[ofEight - 1].send_value(0, True)

            if self._slower == True:
                # self.log_message("Slower! ofEight: "+str((ofEight + 1) * -1))
                self._stop_buttons[ofEight * -1].send_value(127, True)
                nextBtn = (ofEight - 1) * -1
                self._stop_buttons[nextBtn].send_value(0, True)
            elif self._faster == False:
                for index in range(0,8):
                    self._stop_buttons[index].send_value(0, True)

            if self._has_cleared_fx == -1 and self.song().tracks[0].name=='Gangsterish APC':
                self.song().scenes[5].set_fire_button_state(1);
                self._has_cleared_fx = 0
                self._bpm_control.set_mode(int(self._user_variables['DEFAULT_BPM'])-1)
            elif self._has_cleared_fx == 0:
                self.song().scenes[5].set_fire_button_state(0);
                self._has_cleared_fx = 1
                for btn in self._mute_buttons:
                    btn.send_value(0, True)
                for btn in self._solo_buttons:
                    btn.send_value(0, True)
                for btn in self._arm_buttons:
                    btn.send_value(0, True)
                for btn in self._select_buttons:
                    btn.send_value(0, True)

        if quarter_beat != self._current_quarter_beat:
            self._current_quarter_beat = quarter_beat
            if self._tempo_ramp_active and self._tempo_ramp_settings and self.song().is_playing:
                self._tasks.add(self.apply_tempo_ramp)
            if self.song().tracks[3].mute == False:
                if self._mic_flash == 1:
                    for btn in self._mute_buttons:
                        btn.send_value(127, True)
                    for btn in self._solo_buttons:
                        btn.send_value(127, True)
                    for btn in self._arm_buttons:
                        btn.send_value(127, True)
                    self._mic_flash = 0
                else:
                    for btn in self._mute_buttons:
                        btn.send_value(0, True)
                    for btn in self._solo_buttons:
                        btn.send_value(0, True)
                    for btn in self._arm_buttons:
                        btn.send_value(0, True)
                    self._mic_flash = 1

            # self.log_message("Current ebeat: "+str(eight_beat))
            # self.log_message("Current qbeat: "+str(quarter_beat))
            ebeatOfEight = quarter_beat -1
            if (beat % 2):
                ebeatOfEight += 4
            if self._faster == True:
                # self.log_message("Faster! ofEight: "+str(ebeatOfEight))
                self._stop_buttons[ebeatOfEight].send_value(127, True)
                prevBtn = (ebeatOfEight - 1)
                self._stop_buttons[prevBtn].send_value(0, True)

        if eight_beat != self._current_eight_beat:
            self._current_eight_beat = eight_beat
            self._session.recolor_buttons();


    def _setup_mixer_control(self):
        is_momentary = True
        self._mixer = SpecialMixerComponent(8)
        self._mixer.name = 'Mixer'
        self._mixer.master_strip().name = 'Master_Channel_Strip'
        self._mixer.selected_strip().name = 'Selected_Channel_Strip'
        all_select_buttons = []
        for track in range(8):
            strip = self._mixer.channel_strip(track)
            strip.name = 'Channel_Strip_' + str(track)
            volume_control = SliderElement(MIDI_CC_TYPE, track, 7)
            arm_button = ButtonElement(is_momentary, MIDI_NOTE_TYPE, track, 48)
            if self._user_variables['MOMENTARY_SOLO_BUTTONS']=="TRUE":
                solo_button = GMomentaryButtonElement(self, MIDI_NOTE_TYPE, track, 49)
            else:
                solo_button = ButtonElement(is_momentary, MIDI_NOTE_TYPE, track, 49)
            solo_button.name = str(track) + '_Solo_Button'
            if self._user_variables['MOMENTARY_MUTE_BUTTONS']=="TRUE":
                mute_button = GMomentaryButtonElement(self, MIDI_NOTE_TYPE, track, 50)
            else:
                mute_button = ButtonElement(is_momentary, MIDI_NOTE_TYPE, track, 50)
            select_button = GButtonElement(is_momentary, MIDI_NOTE_TYPE, track, 51)
            volume_control.name = str(track) + '_Volume_Control'
            arm_button.name = str(track) + '_Arm_Button'
            mute_button.name = str(track) + '_Mute_Button'
            select_button.name = str(track) + '_Select_Button'

            self._mute_buttons.append(mute_button)
            self._solo_buttons.append(solo_button)
            self._arm_buttons.append(arm_button)
            self._select_buttons.append(select_button)
            all_select_buttons.append(select_button)

            strip.set_volume_control(volume_control)
            strip.set_arm_button(arm_button)
            strip.set_solo_button(solo_button)
            strip.set_mute_button(mute_button)
            strip.set_select_button(None)
            strip.set_shift_button(self._shift_button)
            strip.set_invert_mute_feedback(True)

        crossfader = SliderElement(MIDI_CC_TYPE, 0, 15)
        master_volume_control = SliderElement(MIDI_CC_TYPE, 0, 14)
        master_select_button = GButtonElement(is_momentary, MIDI_NOTE_TYPE, 0, 80)
        prehear_control = EncoderElement(MIDI_CC_TYPE, 0, 47, Live.MidiMap.MapMode.relative_two_compliment)
        crossfader.name = 'Crossfader'
        master_volume_control.name = 'Master_Volume_Control'
        master_select_button.name = 'Master_Select_Button'
        prehear_control.name = 'Prehear_Volume_Control'
        self._mixer.set_crossfader_control(crossfader)
        self._mixer.set_prehear_volume_control(prehear_control)
        self._mixer.master_strip().set_volume_control(master_volume_control)
        self._mixer.master_strip().set_select_button(master_select_button)

        ## Gangsterish
        self._master_mixer = self._mixer.master_strip()._track.mixer_device
        self._bpm = str(self._master_mixer.song_tempo).split('.')[0]
        bpm_control = GBPMComponent(tuple(self._select_buttons), self)
        bpm_control.set_mode_buttons(tuple(self._select_buttons))
        self._bpm_control = bpm_control
        matrix_modes = MatrixModesComponent(self._matrix, self._session, self._session_zoom, self)
        matrix_modes.name = 'Matrix_Modes'
        self._matrix_modes = matrix_modes
        all_select_buttons.append(master_select_button)
        self._shift_modes = GShiftableSelectorComponent(self, tuple(all_select_buttons), tuple(self._stop_buttons), self._matrix, self._session, self._session_zoom, self._mixer, matrix_modes, bpm_control)
        self._shift_modes.name = 'Shift_Modes'
        self._shift_modes.set_mode_toggle(self._shift_button)

    def _setup_custom_components(self):
        self._setup_device_and_transport_control()
        self._setup_global_control()
        if self.song().tracks[0].name=='Gangsterish APC':
            self.song().start_playing()

    def _setup_device_and_transport_control(self):
        is_momentary = True
        device_bank_buttons = []
        device_param_controls = []
        bank_button_labels = ('Clip_Track_Button', 'Device_On_Off_Button', 'Previous_Device_Button', 'Next_Device_Button', 'Detail_View_Button', 'Rec_Quantization_Button', 'Midi_Overdub_Button', 'Metronome_Button')
        for index in range(8):
            device_bank_buttons.append(ButtonElement(is_momentary, MIDI_NOTE_TYPE, 0, 58 + index))
            device_bank_buttons[-1].name = bank_button_labels[index]
            ring_mode_button = ButtonElement(not is_momentary, MIDI_CC_TYPE, 0, 24 + index)
            ringed_encoder = RingedEncoderElement(MIDI_CC_TYPE, 0, 16 + index, Live.MidiMap.MapMode.absolute)
            ringed_encoder.set_ring_mode_button(ring_mode_button)
            ringed_encoder.name = 'Device_Control_' + str(index)
            ring_mode_button.name = ringed_encoder.name + '_Ring_Mode_Button'
            device_param_controls.append(ringed_encoder)

        device = ShiftableDeviceComponent()
        device.name = 'Device_Component'
        device.set_bank_buttons(tuple(device_bank_buttons))
        device.set_shift_button(self._shift_button)
        device.set_parameter_controls(tuple(device_param_controls))
        device.set_on_off_button(device_bank_buttons[1])
        self.set_device_component(device)
        detail_view_toggler = DetailViewCntrlComponent()
        detail_view_toggler.name = 'Detail_View_Control'
        detail_view_toggler.set_shift_button(self._shift_button)
        detail_view_toggler.set_device_clip_toggle_button(device_bank_buttons[0])
        detail_view_toggler.set_detail_toggle_button(device_bank_buttons[4])
        detail_view_toggler.set_device_nav_buttons(device_bank_buttons[2], device_bank_buttons[3])
        transport = ShiftableTransportComponent()
        transport.name = 'Transport'

        # Gangsterish
        vu = VUMeters(self,False)
        vu0 = SimpleVU(self,4,4)
        vu1 = SimpleVU(self,5,4)
        vu2 = SimpleVU(self,6,4)
        vu3 = SimpleVU(self,7,4)
        vu4 = SimpleVU(self,8,4)
        vu5 = SimpleVU(self,9,4)
        vu6 = SimpleVU(self,10,4)
        vu7 = SimpleVU(self,11,4)

        play_button = ButtonElement(is_momentary, MIDI_NOTE_TYPE, 0, 91)
        stop_button = ButtonElement(is_momentary, MIDI_NOTE_TYPE, 0, 92)
        record_button = ButtonElement(is_momentary, MIDI_NOTE_TYPE, 0, 93)
        nudge_up_button = ButtonElement(is_momentary, MIDI_NOTE_TYPE, 0, 100)
        nudge_down_button = ButtonElement(is_momentary, MIDI_NOTE_TYPE, 0, 101)
        tap_tempo_button = ButtonElement(is_momentary, MIDI_NOTE_TYPE, 0, 99)
        play_button.name = 'Play_Button'
        stop_button.name = 'Stop_Button'
        record_button.name = 'Record_Button'
        nudge_up_button.name = 'Nudge_Up_Button'
        nudge_down_button.name = 'Nudge_Down_Button'
        tap_tempo_button.name = 'Tap_Tempo_Button'
        transport.set_shift_button(self._shift_button)
        transport.set_play_button(play_button)
        transport.set_stop_button(stop_button)
        transport.set_record_button(record_button)
        transport.set_nudge_buttons(nudge_up_button, nudge_down_button)
        transport.set_tap_tempo_button(tap_tempo_button)
        transport.set_quant_toggle_button(device_bank_buttons[5])
        transport.set_overdub_button(device_bank_buttons[6])
        transport.set_metronome_button(device_bank_buttons[7])
        bank_button_translator = ShiftTranslatorComponent()
        bank_button_translator.set_controls_to_translate(tuple(device_bank_buttons))
        bank_button_translator.set_shift_button(self._shift_button)

    def _setup_global_control(self):
        is_momentary = True
        global_bank_buttons = []
        global_param_controls = []
        for index in range(8):
            ring_button = ButtonElement(not is_momentary, MIDI_CC_TYPE, 0, 56 + index)
            ringed_encoder = RingedEncoderElement(MIDI_CC_TYPE, 0, 48 + index, Live.MidiMap.MapMode.absolute)
            ringed_encoder.name = 'Track_Control_' + str(index)
            ring_button.name = ringed_encoder.name + '_Ring_Mode_Button'
            ringed_encoder.set_ring_mode_button(ring_button)
            global_param_controls.append(ringed_encoder)

        global_bank_buttons = []
        global_bank_labels = ('Pan_Button', 'Send_A_Button', 'Send_B_Button', 'Send_C_Button')
        for index in range(4):
            global_bank_buttons.append(ButtonElement(is_momentary, MIDI_NOTE_TYPE, 0, 87 + index))
            global_bank_buttons[-1].name = global_bank_labels[index]

        encoder_modes = EncModeSelectorComponent(self._mixer)
        encoder_modes.name = 'Track_Control_Modes'
        encoder_modes.set_modes_buttons(global_bank_buttons)
        encoder_modes.set_controls(tuple(global_param_controls))
        global_translation_selector = ChannelTranslationSelector()
        global_translation_selector.name = 'Global_Translations'
        global_translation_selector.set_controls_to_translate(tuple(global_param_controls))
        global_translation_selector.set_mode_buttons(tuple(global_bank_buttons))

    def _product_model_id_byte(self):
        return 115

    # Gangsterish
    def get_user_settings(self):
        if not hasattr(self, "_user_variables"):
            self._user_variables = {}
        script_path = ''
        for path in sys.path:
            if 'MIDI Remote Scripts' in path:
                script_path = path
                break
        user_file = script_path + FOLDER + 'Settings.txt'
        for line in open(user_file): 
            line = line.rstrip('\n')
            if not line.startswith(('#', '"',)) and not line == '' and '=' in line:
                var_data = line.split('=')
                if len(var_data) >= 2 and not ';' in var_data[1] and not '%' in var_data[1] and not '=' in var_data[1]:
                    self._user_variables[var_data[0].strip()] = var_data[1].strip()

    def adjust_tempo(self, args):
        self._tempo_ramp_active = False
        self._tempo_ramp_settings = []
        if IS_LIVE_9:
            arg_array = args.split()
            if len(arg_array) == 2:
                try:
                    ramp_factor = float("%.2f" % (int(arg_array[0]) * self.song().signature_numerator))
                    target_tempo = float("%.2f" % float(arg_array[1]))
                    if target_tempo >= 20.0 and target_tempo <= 999.0:
                        self._tempo_ramp_settings = [target_tempo, (target_tempo - self.song().tempo) / ramp_factor]
                        self._tempo_ramp_active = True
                except: pass
        else:
            try:
                self.song().tempo = float(args)
            except: pass
        
    def apply_tempo_ramp(self, arg=None):
        """ Apply tempo smoothing """
        target_reached = False
        if self._tempo_ramp_settings[1] > 0:
            target_reached = self._tempo_ramp_settings[0] <= self.song().tempo
        else:
            target_reached = self._tempo_ramp_settings[0] >= self.song().tempo
        if target_reached:
            self.song().tempo = self._tempo_ramp_settings[0]
            self._tempo_ramp_active = False
            self._tasks.kill()
            self._tasks.clear()
        else:
            self.song().tempo += self._tempo_ramp_settings[1] 
