import Live
if Live.Application.get_application().get_major_version() == 9:
    from APC80_40 import APC80_40
else:
	from APC80_408 import APC80_40


def create_instance(c_instance):
    """ Creates and returns the Gangsterish APC80_40 script """
    return APC80_40(c_instance)

if Live.Application.get_application().get_major_version() == 9:
	from _Framework.Capabilities import *

def get_capabilities():
    return {CONTROLLER_ID_KEY: controller_id(vendor_id=2536, product_ids=[115], model_name='Akai APC40'),
     PORTS_KEY: [inport(props=[NOTES_CC, SCRIPT, REMOTE]), outport(props=[SCRIPT, REMOTE])]}