from _Framework.ModeSelectorComponent import ModeSelectorComponent 
from _Framework.ButtonElement import ButtonElement 
from _Framework.MixerComponent import MixerComponent 
from _Framework.ButtonMatrixElement import ButtonMatrixElement
from _Framework.ControlSurface import ControlSurface

class GBPMComponent(ModeSelectorComponent):
    ' SelectorComponent that controls BPM '
    __module__ = __name__

    def __init__(self, buttons, parent):
        ModeSelectorComponent.__init__(self)
        self._controls = None
        self._buttons = buttons
        self._button_matrix = ButtonMatrixElement()
        button_row = []
        for track_index in range(8):
            button = self._buttons[track_index]
            button_row.append(button)
        self._button_matrix.add_row(tuple(button_row))
        self._mode_index = 0
        self._last_bpm = -1
        self._parent = parent
        
    def disconnect(self):
        for button in self._modes_buttons:
            button.remove_value_listener(self._mode_value)
        self._controls = None
        self._session = None
        self._session_zoom = None
        self._buttons = None
        self._button_matrix = None
        ModeSelectorComponent.disconnect(self)

    def set_mode(self, mode):
        assert isinstance(mode, int)
        assert (mode in range(self.number_of_modes()))
        if (self._mode_index != mode):
            self._last_mode = 0
            self._mode_index = mode
            self._set_modes()

    def set_mode_buttons(self, buttons):
        assert isinstance(buttons, (tuple,
                                    type(None)))
        for button in self._modes_buttons:
            button.remove_value_listener(self._mode_value)

        self._modes_buttons = []
        if (buttons != None):
            for button in buttons:
                assert isinstance(button, ButtonElement)
                identify_sender = True
                button.add_value_listener(self._mode_value, identify_sender)
                self._modes_buttons.append(button)
            for index in range(len(self._modes_buttons)):
                if (index == self._mode_index):
                    self._modes_buttons[index].turn_on()
                else:
                    self._modes_buttons[index].turn_off()

    def _mode_value(self, value, sender):
        assert (len(self._modes_buttons) > 0)
        assert isinstance(value, int)
        assert isinstance(sender, ButtonElement)
        assert (self._modes_buttons.count(sender) == 1)
        if self.is_enabled():
            if ((value is not 0) or (not sender.is_momentary())):
                self.set_mode(self._modes_buttons.index(sender))                    

    def number_of_modes(self):
        return 8

    def update(self):
        pass

    def get_mode_index_value(self):
        return self._mode_index

    def _set_modes(self):
        if self.is_enabled():
            assert (self._mode_index in range(self.number_of_modes()))
            for index in range(len(self._modes_buttons)):
                if (index == self._mode_index):
                    self._modes_buttons[index].turn_on()
                else:
                    self._modes_buttons[index].turn_off()
            if (self._mode_index == 0):
                self._parent.adjust_tempo(self._parent._user_variables['BPM_1'])
            elif (self._mode_index == 1):
                self._parent.adjust_tempo(self._parent._user_variables['BPM_2'])
            elif (self._mode_index == 2):
                self._parent.adjust_tempo(self._parent._user_variables['BPM_3'])
            elif (self._mode_index == 3):
                self._parent.adjust_tempo(self._parent._user_variables['BPM_4'])
            elif (self._mode_index == 4):
                self._parent.adjust_tempo(self._parent._user_variables['BPM_5'])
            elif (self._mode_index == 5):
                self._parent.adjust_tempo(self._parent._user_variables['BPM_6'])
            elif (self._mode_index == 6):
                self._parent.adjust_tempo(self._parent._user_variables['BPM_7'])
            elif (self._mode_index == 7):
                self._parent.adjust_tempo(self._parent._user_variables['BPM_8'])
            else:
                pass
