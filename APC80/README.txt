Gangsterish Template Suite
APC80, APC40_DJ, APC20_DJ Control Surface Scripts 


http://apc80.com


Version 3.0 - Apr-19-2013


by Gangsterish - http://gangsterish.com
Features
 * Super easy and fast visual navigation of very large live sets, easily supports up to 1,000 scenes. Never look at your laptop during a set again
 * Momentary Solo and Mute buttons
 * Fully mappable faders (APC80 Only)
 * Custom DJ effects racks with 8 effects per deck (APC80, APC40_DJ Only)
 * Two decks setup and ready to go with stems
 * Multiple cross fader modes (APC80, APC40_DJ Only)
 * VU Meters for each deck AND every track (every track APC80, APC40_DJ only)
 * Smart EQ (APC80, APC40_DJ Only)
 * Smart Volume
 * Smart Headphone Cueing
 * Scratch Effects
 * Vinyl Stop
 * Lot's more to come, and updates are always free.


This script requires a working copy of Ableton Live 9.* or higher.


These scripts optimize the APC 80 (Akai APC 40 and Akai APC 20 used in tandem), APC40 and APC20 to be effective live performance tools, going far beyond their stock abilities.


Setting a clip in Ableton to either the very top left red color, or the reddish-brown directly below will make the button on the APC light up red. ANY OTHER COLOR will make the button light up Yellow.


The sample clips are almost always best triggered with Gate mode, so they stop when you release the button. Though sometimes it's fun to have a nice long sample on Trigger mode that you can hit between tracks or during fills as well.


I included one of my tracks with warp marks, stems, and samples, so you can see how I use it. I also duplicated this track vertically a bunch so you can see how the scrolling should work. Just delete them and put your tracks where you want them.


The Up/Down scroll buttons are set to scroll by page (6 rows) rather than row by default. I find it much easier to arrange things by Page than by Row. It's not only easier to remember where you/tracks are in a set, but to navigate to them quickly. Because you will never scroll sideways in this set, the right/left buttons are mapped instead to a vinyl stop effect for the respective side. The speed of the vinyl stop is tweaked by the Master fader (try starting at the bottom and pushing it to the top) and the brake size is tweaked by the cue knob.


I have separated the pages in this version, this makes it much easier to not only lay your set out, but to setup looping follow-actions. You’ll notice the scenes that are greyed out on the right and don’t have stop buttons. These are page separators. So now you can easily lay out a set without having your APC plugged in or having to count rows.


The left vertical row of knobs from the top down is a one knob filter (left is lows only, right is highs only). Then a smart high, mid, and low pass filter, turning left cuts said frequency, turning right boosts it slightly while lowering that range from the other deck. The second row of vertical knobs are smart gain (wiggle room in the middle with no effect, fully left kills sound, fully right boosts a ton), delay, reverb, and beat repeat. The right deck mirrors these on the other side, with the mixer knobs for deck B on the far right, and effects next to them to the left. (Look at the layout graphic for an eaiser time locating them.)


I find it best and easiest to use identical decks on both sides. To start, import a track, set it up how you want it on deck A, then duplicate it to the other side. This opens up a lot of possibilities with live remixing of tracks, as well as makes it easier to navigate through the set.


The volume faders don't control the volume of the track actually, but instead a crossing high/low pass filter. This lets the mids in first, and then the highs and lows at the end. It sounds much nicer than normal volume. They are meant to be slammed at the top to be a fully loud signal.


The track selection buttons are BPM controls. You can configure these to any BPM you like by renaming the scenes at the very bottom of the Live set. The defaults from left to right are: 85, 95, 110, 130, 140, 150. I find this covers most of the styles of music I play. This also allows for quick, slowing or speeding up effects. (Track select buttons 7 and 8 currently do nothing, feel free to map it to something cool!)


All effects are tweaked by the master fader and cue level knob. The button below the low filter on either deck is a fade to gray effect, best to start with the fader at the bottom and raise it slowly. The button next to it, below the beat repeat knob, is an LFO driven gate, with the Master slider affecting depth, and cue level affecting rate.


You can change cross fader modes by selecting either Detail View, Rec Quantization, MIDI Overdub, or Metronome. Pressing one will turn it on, but not turn the old one off, so you need to press the button of the mode you want to go to, and the mode you're currently on simultaneously to achieve a nice transition. If you have no sound, you may have turned off all cross fader modes, and pushing any button should bring it back on. If you don't like this, like any feature in the template, remap the midi mappings to something else!


Detail View selects a quick cut cross fader. Rec quantization will high pass Deck A and low pass Deck B. MIDI Overdub will high pass Deck B and low pass Deck A. Metronome uses a classic slow cross fader.


The template features ‘smart cueing’, if you have the crossfader to the left of center, you will hear the right deck in your headphones. Likewise if you have the crossfader to the right of center, you will hear only the left deck in your headphones. You can also mix in the currently selected deck by tweaking the cue level, allowing for in-headphone mixing (useful when you can’t hear the speakers well or there is a delay).


For full layout info, watch the videos at http://youtube.com/MrGangsterish and/or check out the included layout images.
Installation
MACINTOSH:
        - Open /Applications/Live 8.*.*/
        - Right click on Live.App
        - Select "Show package contents"
        - Drag the APC80_40 and APC80_20, APC40_DJ, and/or APC20_DJ folders into App-Resources/MIDI Remote Scripts/
        - Drag Gangsterish.ask into App-Resources/Skins/


WINDOWS:
        - Locate the folder for your CURRENT version of Ableton inside of C:\ProgramFiles folder.
        - Open your version of Abelton and navigate to the subdirectory: Resources\Midi Remote Scripts.
        - Drag and drop the APC80_40, APC80_20, APC40_DJ, and APC20_DJ folders into there.
        - Drag Gangsterish.ask into Resources\Skins




        1. Start Ableton
        2. Open Preferences
        3. Under MIDI:
                For whichever surface(s) you will be using, turn on ‘Track’ and ‘Remote’
                For APC80:
                        Set APC 40 Control Surface drop down to APC80_40
                        Set APC 20 Control Surface drop down to APC80_20
                For APC40:
                        Set APC 40 Control Surface drop down to APC40_DJ
                For APC20:
                        Set APC 20 Control Surface drop down to APC20_DJ
        4. Under Record-Warp-Launch:
                Turn 'Select on Launch' Off.
        5. Under Look Feel:
                Set Skin to Gangsterish
        6. Open APC80.als, APC40_DJ.als or APC20_DJ.als
                Send the Audio Out in the I-O section of the Cue A and B tracks, to your cue audio channel, to enable auto cueing.
                Wiggle all your knobs and faders to make sure they are where they should be.
                All faders should be at the top.
                Your knobs should be positioned like such to start:
                        Very left and right columns - centered
                        Middle two columns - Top knob, gain should be centered. Bottom three knobs, FX, should start all the way down).
                APC80 Only:
                        If the right deck (APC20) is controlling the left side, Deck A, they connected in the wrong order. Open Preferences again, set APC 20 Control Surface to None, then back to APC80_20 again to reset it.
        7. Click the ‘Clear FX’ scene button at the very top of the set.
        8. Import and warp your music
        9. Change the world
Bonus
The APC80 template includes stems and cue points set up already for my track Walk Like a Gangsta. The APC40_DJ and APC20_DJ templates come with a track as well. Feel free to remix, delete, or do whatever with these. They are intended only to show you how the template should work. In fact everything in this template is made for you to tweak. Add your own samples to the scene launch buttons, use your own effects. Have fun with it!


Thanks
        The amazing Ableton / EDM community
        ill.Gates                                                 http://illgates.com/
        Tom Cosm                                                http://www.cosm.co.nz/
        Will Marshall                                                 http://willmarshall.me/
        Hanz Petrov                                                http://remotescripts.blogspot.com/
        Tarekith                                                http://tarekith.com/


Changelog
1. 1.0.0 - Initial release
2. 1.0.1 - Smart cueing (Cue deck automatically plays the inverse of the cross fader)
1. Greatly improved effects - Vinyl Stop, Fade to Gray are all now tweaked by the master fader.
1. Removed frequency shifter, added gain knob.
2. Added smart EQ knobs. Boosting frequencies on one deck, cuts them on the other.
1. 1.0.2 - Master beat repeat tempo is now tweaked (like all effects) on the Master fader. Fixed a few issues, general clean up.
2. 1.0.3 - Added APC20 template
3. 2.0 - Added VU meters.
1. Added scratch effects
2. Reworked all effects and EQs
3. Improved volume sliders
4. Added Mic and external deck inputs
5. Improved audio flow and cueing
6. Added Visible metronome
7. Improved theme colors readability
8. Extra cross fader modes
1. Quick Cut
2. Low passes left track as it brings in highs, then lows of right track
3. Low passes right track as it brings in highs, then lows of left track
4. Use smart volume rather than straight volume
5. Standard
1. Visible BPM Indicator
2. Individual channel VU. If there's sound, light up that channel's record button.
1. 3.0 - Live 9 Update
1. Button color controlled by Ableton Clip Color
2. Separate deck scrolling (APC80 only)
3. Improved scrolling
4. Improved zooming
5. In-headphone cue mixing ability
6. Improved FX