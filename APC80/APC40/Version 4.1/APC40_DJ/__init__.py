import Live
from APC40_DJ import APC40_DJ


def create_instance(c_instance):
    """ Creates and returns the Gangsterish APC40_DJ script """
    return APC40_DJ(c_instance)

if Live.Application.get_application().get_major_version() == 9:
	from _Framework.Capabilities import *

def get_capabilities():
    return {CONTROLLER_ID_KEY: controller_id(vendor_id=2536, product_ids=[115], model_name='Akai APC40'),
     PORTS_KEY: [inport(props=[NOTES_CC, SCRIPT, REMOTE]), outport(props=[SCRIPT, REMOTE])]}