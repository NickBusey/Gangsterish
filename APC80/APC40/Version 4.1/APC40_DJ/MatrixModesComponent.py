from _Framework.ModeSelectorComponent import ModeSelectorComponent 
from _Framework.ButtonElement import ButtonElement 
from _Framework.MixerComponent import MixerComponent 
from _Framework.ControlSurface import ControlSurface

# Gangsterish
from GButtonMatrixElement import GButtonMatrixElement
from MatrixMaps import *

class MatrixModesComponent(ModeSelectorComponent):
    ' SelectorComponent that assigns matrix to different functions '
    __module__ = __name__

    def __init__(self, matrix, session, zooming, parent):
        assert isinstance(matrix, GButtonMatrixElement)
        ModeSelectorComponent.__init__(self)
        self._controls = None
        self._session = session
        self._session_zoom = zooming
        self._matrix = matrix
        # self._track_stop_buttons = stop_buttons
        # self._stop_button_matrix = GButtonMatrixElement() #new dummy matrix for stop buttons, to allow note mode/user mode switching
        # button_row = []
        # for track_index in range(8):
        #     button = self._track_stop_buttons[track_index]
        #     button_row.append(button)
        # self._stop_button_matrix.add_row(tuple(button_row))
        self._mode_index = 8
        self._last_mode = 0
        self._parent = parent
        self._parent.set_pad_translations(PAD_TRANSLATIONS) #comment out to remove Drum Rack mapping

        
    def disconnect(self):
        for button in self._modes_buttons:
            button.remove_value_listener(self._mode_value)
        self._controls = None
        self._session = None
        self._session_zoom = None
        self._matrix = None
        self._track_stop_buttons = None
        self._stop_button_matrix = None
        ModeSelectorComponent.disconnect(self)

        
    def set_mode(self, mode): #override ModeSelectorComponent set_mode, to avoid flickers
        assert isinstance(mode, int)
        assert (mode in range(self.number_of_modes()))
        if (self._mode_index != mode):
            self._last_mode = 0 # self._mode_index # keep track of previous mode, to allow refresh after Note Mode only
            self._mode_index = mode
            self._set_modes()
            
            
    def set_mode_buttons(self, buttons):
        # assert isinstance(buttons, (tuple, type(None)))
        for button in self._modes_buttons:
            button.remove_value_listener(self._mode_value)

        self._modes_buttons = []
        if (buttons != None):
            for button in buttons:
                # assert isinstance(button, ButtonElement)
                identify_sender = True
                button.add_value_listener(self._mode_value, identify_sender)
                self._modes_buttons.append(button)
            for index in range(len(self._modes_buttons)):
                if (index == self._mode_index):
                    self._modes_buttons[index].turn_on()
                else:
                    self._modes_buttons[index].turn_off()


    def _mode_value(self, value, sender):
        assert (len(self._modes_buttons) > 0)
        assert isinstance(value, int)
        assert isinstance(sender, ButtonElement)
        assert (self._modes_buttons.count(sender) == 1)
        if self.is_enabled():
            if ((value is not 0) or (not sender.is_momentary())):
                self.set_mode(self._modes_buttons.index(sender))                    

    def number_of_modes(self):
        return 9
    
    def update(self):
        pass

    def get_mode_index_value(self):
        return self._mode_index
    
    def _set_modes(self):
        if self.is_enabled():
            self._session.set_allow_update(False)
            self._session_zoom.set_allow_update(False)
            assert (self._mode_index in range(self.number_of_modes()))
            for index in range(len(self._modes_buttons)):
                if (index == self._mode_index):
                    self._modes_buttons[index].turn_on()
                else:
                    self._modes_buttons[index].turn_off()
            # self._session.set_stop_track_clip_buttons(tuple(self._track_stop_buttons))            
            # for track_index in range(8):
            #     button = self._track_stop_buttons[track_index]
            #     button.use_default_message()
            #     button.set_enabled(True)
            #     button.set_force_next_value()
            #     button.send_value(0)
            self._session_zoom.set_enabled(True)
            self._session.set_enabled(True)
            self._session.set_show_highlight(True)
            self._session_zoom.set_zoom_button(self._parent._shift_button)
            for scene_index in range(5):
                scene = self._session.scene(scene_index) 
                for track_index in range(8):                
                    button = self._matrix.get_button(track_index, scene_index)
                    button.use_default_message()
                    clip_slot = scene.clip_slot(track_index)
                    clip_slot.set_launch_button(button)
                    button.set_enabled(True)
                
            if (self._mode_index == 0): 
                self._set_note_mode(PATTERN_1, CHANNEL_1, NOTEMAP_1)

                # for scene_index in range(5):
                #     scene = self._session.scene(scene_index)
                #     for track_index in range(8):
                #         clip_slot = scene.clip_slot(track_index)
                #         button = self._parent._matrix.get_button(track_index, scene_index)
                #         clip_slot.set_launch_button(None)
                #         # button.set_identifier(self._parent._matrix_modes._notemap[scene_index][track_index])
                #         # # button.set_on_off_values(self._parent._matrix_modes._pattern[scene_index][track_index], 0)
                #         # button.set_force_next_value()
                #         # button.turn_on()
                #         # button.set_enabled(False)
                #         def callback(self,value):
                #             self._parent.log_message("Value: ",value)
                #             track = self.song().tracks[4]
                #             clip_slot_index = track.playing_slot_index
                #             clip = track.clip_slots[clip_slot_index].clip
                #             if (clip):
                #                 console.log("Got clip, do it!")
                #                 clip.move_playing_pos(-8)

                #         self._parent.log_message("Setting callback")
                #         button.add_value_listener(callback)
            elif (self._mode_index == 1):
                self._set_note_mode(PATTERN_2, CHANNEL_2, NOTEMAP_2)
            elif (self._mode_index == 2):
                self._set_note_mode(PATTERN_3, CHANNEL_3, NOTEMAP_3)
            elif (self._mode_index == 3):
                self._set_note_mode(PATTERN_4, CHANNEL_4, NOTEMAP_4)
            elif (self._mode_index == 4):
                self._set_note_mode(PATTERN_5, CHANNEL_5, NOTEMAP_5)
            elif (self._mode_index == 5):
                self._set_note_mode(PATTERN_6, CHANNEL_6, NOTEMAP_6)
            elif (self._mode_index == 6):
                self._mode_index = 8
                self._session.set_offsets(self._session.track_offset(), max(0, self._session.scene_offset() - self._session._scene_banking_increment))
            elif (self._mode_index == 7):
                self._mode_index = 8
                self._session.set_offsets(self._session.track_offset(), self._session.scene_offset() + self._session._scene_banking_increment)
            elif (self._mode_index == 8): #Clip Launch
                self._session_zoom._on_zoom_value(1) #zoom out
            else:
                return
            self._session.set_allow_update(True)
            self._session_zoom.set_allow_update(True)
            self._session.recolor_buttons()
        else:
            if (self._mode_index == 8): #Clip Launch
                self._session_zoom._on_zoom_value(1) #zoom out
                self._session.recolor_buttons()


    def _set_note_mode(self, pattern, channel, notemap):
        self._session_zoom.set_zoom_button(None)
        self._session_zoom.set_enabled(False)
        self._pattern = pattern
        self._channel = channel
        self._notemap = notemap
        self._session.set_enabled(True)
        self._session.set_show_highlight(True)
