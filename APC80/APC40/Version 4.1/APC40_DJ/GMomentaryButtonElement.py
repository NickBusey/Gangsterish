#Embedded file name: /Users/versonator/Hudson/live/Projects/AppLive/Resources/MIDI Remote Scripts/_Framework/ButtonElement.py
import Live
from _Framework.ButtonElement import ButtonElement

class GMomentaryButtonElement(ButtonElement):
    """
    Class representing a button on the controller
    Gangsterish, makes it momentary
    """

    def __init__(self, parent, msg_type, channel, identifier):
        super(GMomentaryButtonElement, self).__init__(True, msg_type, channel, identifier)
        self.__is_momentary = True
        self._parent = parent
        self._last_received_value = -1

    def turn_on(self):
        self.send_value(127)
        # self._parent.log_message("turn on")

    def turn_off(self):
        self.send_value(0)
        # self._parent.log_message("turn off")

    def receive_value(self, value):
        pressed_before = self.is_pressed()
        self._last_received_value = value
        super(GMomentaryButtonElement, self).receive_value(value)
        # self._parent.log_message("_last_received_value: "+str(self._last_received_value))
        if (self._last_received_value == 0):
            # self._parent.log_message("Finger release, switch mode")
            self.send_value(127, True)
            self.receive_value(127)