# emacs-mode: -*- python-*-
# -*- coding: utf-8 -*-

from ControlElement import ControlElement 
class NotifyingControlElement(ControlElement):
    ' Class representing control elements that can send values '


    def __init__(self):
        ControlElement.__init__(self)
        self._value_notifications = []


    def disconnect(self):
        ControlElement.disconnect(self)
        self._value_notifications = []


    def add_value_listener(self, callback, identify_sender = False):
        ' Registers a callback that is triggerd when a message is received '
        assert (callback != None)
        assert (dir(callback).count('__call__') is 1)
        assert (not self.value_has_listener(callback))
        for notification in self._value_notifications:
            assert (notification['Callback'] != callback)
        self._value_notifications.append({'Callback': callback,
         'Identify': identify_sender})


    def remove_value_listener(self, callback):
        ' Unregisters a forwarding callback '
        assert (callback != None)
        assert (dir(callback).count('__call__') is 1)
        assert self.value_has_listener(callback)
        entry_removed = False
        for notification in self._value_notifications:
            if (notification['Callback'] == callback):
                self._value_notifications.remove(notification)
                entry_removed = True
        assert entry_removed


    def value_has_listener(self, callback):
        ' returns whether the given callback is already registered '
        assert (callback != None)
        assert (dir(callback).count('__call__') is 1)
        callback_found = False
        for notification in self._value_notifications:
            if (notification['Callback'] == callback):
                callback_found = True
                break

        return callback_found



# local variables:
# tab-width: 4
