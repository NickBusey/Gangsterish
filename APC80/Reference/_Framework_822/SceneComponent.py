# --== Decompile ==--

import Live
from CompoundComponent import CompoundComponent
from ControlSurfaceComponent import ControlSurfaceComponent
from ClipSlotComponent import ClipSlotComponent
from ButtonElement import ButtonElement

class SceneComponent(CompoundComponent):
    __doc__ = ' Class representing a scene in Live '
    def __init__(self, num_slots, tracks_to_use_callback):
        assert (num_slots != None)
        assert isinstance(num_slots, int)
        assert (num_slots >= 0)
        assert tracks_to_use_callback != None
        assert dir(tracks_to_use_callback).count('im_func') is 1
        CompoundComponent.__init__(self)
        self._scene = None
        self._clip_slots = []
        self._tracks_to_use_callback = tracks_to_use_callback
        for index in range(num_slots):
            new_slot = self._create_clip_slot()
            self._clip_slots.append(new_slot)
            self.register_components(new_slot)
        self._launch_button = None
        self._triggered_value = 127
        self._track_offset = 0
        return None

    
    def disconnect(self):
        CompoundComponent.disconnect(self)
        if self._scene != None:
            self._scene.remove_is_triggered_listener(self._on_is_triggered_changed)
        if self._launch_button != None:
            self._launch_button.remove_value_listener(self._launch_value)
            self._launch_button = None
        self._scene = None
        self._clip_slots = None
        self._tracks_to_use_callback = None
        return None

    
    def on_enabled_changed(self):
        self.update()

        
    def on_track_list_changed(self):
        self.update()

        
    def set_scene(self, scene):
        assert ((scene == None) or isinstance(scene, Live.Scene.Scene))
        if scene != self._scene:
            if self._scene != None:
                self._scene.remove_is_triggered_listener(self._on_is_triggered_changed)
            self._scene = scene
            if self._scene != None:
                self._scene.add_is_triggered_listener(self._on_is_triggered_changed)
            self.update()
        return None

    
    def set_launch_button(self, button):
        assert ((button == None) or isinstance(button, ButtonElement))
        if button != self._launch_button:
            if self._launch_button != None:
                self._launch_button.remove_value_listener(self._launch_value)
            self._launch_button = button
            if self._launch_button != None:
                self._launch_button.add_value_listener(self._launch_value)
            self.update()
        return None

    
    def set_track_offset(self, offset):
        assert (offset != None)
        assert isinstance(offset, int)
        assert offset >= 0
        if offset != self._track_offset:
            self._track_offset = offset
            self.update()
        return None

    
    def set_triggered_value(self, value):
        assert (value in range(128))
        self._triggered_value = value

        
    def clip_slot(self, index):
        assert (index != None)
        assert isinstance(index, int)
        assert (index in range(len(self._clip_slots)))
        return self._clip_slots[index]

    
    def update(self):
        if self._allow_updates:
            if self._scene != None and self.is_enabled():
                clip_index = self._track_offset
                tracks = self.song().tracks
                clip_slots = self._scene.clip_slots
                if self._track_offset > 0:
                    real_offset = 0
                    visible_tracks = 0
                    while visible_tracks < self._track_offset and len(tracks) > real_offset:
                        if tracks[real_offset].is_visible:
                            visible_tracks += 1
                        real_offset += 1
                    clip_index = real_offset
                for slot in self._clip_slots:
                    while len(tracks) > clip_index and not tracks[clip_index].is_visible:
                        clip_index += 1
                    if len(clip_slots) > clip_index:
                        slot.set_clip_slot(clip_slots[clip_index])
                    else:
                        slot.set_clip_slot(None)
                    clip_index += 1
                self._on_is_triggered_changed()
            else:
                for slot in self._clip_slots:
                    slot.set_clip_slot(None)
                if self.is_enabled() and self._launch_button != None:
                    self._launch_button.turn_off()
            self._rebuild_callback()
        else:
            self._update_requests += 1
        return None

    
    def _launch_value(self, value):
        assert (self._launch_button != None)
        assert (value in range(128))
        if self._scene != None and self.is_enabled():
            launched = False
            if self._launch_button.is_momentary():
                self._scene.set_fire_button_state(value != 0)
                launched = (value != 0)
            elif value != 0:
                self._scene.fire()
                launched = True
            if launched and self.song().select_on_launch:
                self.song().view.selected_scene = self._scene


    def _on_is_triggered_changed(self):
        assert (self._scene != None)
        if self.is_enabled() and self._launch_button != None:
            if self._scene.is_triggered:
                self._launch_button.send_value(self._triggered_value)
            else:
                self._launch_button.turn_off()


    def _create_clip_slot(self):
        return ClipSlotComponent()



