# --== Decompile ==--

import Live
from ControlSurfaceComponent import ControlSurfaceComponent
from ButtonElement import ButtonElement
class ModeSelectorComponent(ControlSurfaceComponent):
    __doc__ = ' Class for switching between modes, handle several functions with few controls '
    def __init__(self):
        ControlSurfaceComponent.__init__(self)
        self._modes_buttons = []
        self._mode_toggle = None
        self._mode_listeners = []
        self._ModeSelectorComponent__mode_index = -1
        return None

    def _get_protected_mode_index(self):
        return self._ModeSelectorComponent__mode_index

    def _set_protected_mode_index(self, mode):
        assert isinstance(mode, int)
        self._ModeSelectorComponent__mode_index = mode
        for listener in self._mode_listeners:
            listener()

    _mode_index = property(_get_protected_mode_index, _set_protected_mode_index)

    def _get_public_mode_index(self):
        return self._ModeSelectorComponent__mode_index

    def _set_public_mode_index(self, mode):
        assert False


    mode_index = property(_get_public_mode_index, _set_public_mode_index)

    def disconnect(self):
        if self._mode_toggle != None:
            self._mode_toggle.remove_value_listener(self._toggle_value)
            self._mode_toggle = None
        self._modes_buttons = None
        self._mode_listeners = None
        return None

    def on_enabled_changed(self):
        self.update()

    def set_mode_toggle(self, button):
        assert ((button == None) or isinstance(button, ButtonElement))
        if (self._mode_toggle != None):
            self._mode_toggle.remove_value_listener(self._toggle_value)
        self._mode_toggle = button
        if (self._mode_toggle != None):
            self._mode_toggle.add_value_listener(self._toggle_value)
        return None


    def set_mode_buttons(self, buttons):
        assert (buttons != None)
        assert isinstance(buttons, tuple)
        assert ((len(buttons) - 1) in range(16))
        for button in buttons:
            assert isinstance(button, ButtonElement)
            identify_sender = True
            button.add_value_listener(self._mode_value, identify_sender)
            self._modes_buttons.append(button)
        self.set_mode(0)


    def set_mode(self, mode):
        assert isinstance(mode, int)
        assert (mode in range(self.number_of_modes()))
        if (self._mode_index != mode):
            self._mode_index = mode
            self.update()

    def number_of_modes(self):
        debug_print('number_of_modes is abstract. Forgot to override it?')
        assert False

    def mode_index_has_listener(self, listener):
        return listener in self._mode_listeners

    def add_mode_index_listener(self, listener):
        assert (listener not in self._mode_listeners)
        self._mode_listeners.append(listener)

    def remove_mode_index_listener(self, listener):
        assert (listener in self._mode_listeners)
        self._mode_listeners.remove(listener)

    def update(self):
        debug_print('update is abstract. Forgot to override it?')
        assert False

    def _mode_value(self, value, sender):
        assert (len(self._modes_buttons) > 0)
        assert isinstance(value, int)
        assert isinstance(sender, ButtonElement)
        assert (self._modes_buttons.count(sender) == 1)
        if ((value is not 0) or (not sender.is_momentary())):
            self.set_mode(self._modes_buttons.index(sender))

    def _toggle_value(self, value):
        assert (self._mode_toggle != None)
        assert isinstance(value, int)
        if ((value is not 0) or (not self._mode_toggle.is_momentary())):
            self.set_mode(((self._mode_index + 1) % self.number_of_modes()))



