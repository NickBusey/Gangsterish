# emacs-mode: -*- python-*-
# -*- coding: utf-8 -*-

from ModeSelectorComponent import ModeSelectorComponent 
from ControlSurfaceComponent import ControlSurfaceComponent 
from InputControlElement import InputControlElement 
from ButtonElement import ButtonElement 
class SwitchModeSelectorComponent(ModeSelectorComponent):
    ' Class that handles modes by enabling and disabling components '


    def __init__(self):
        ModeSelectorComponent.__init__(self)
        self._components_per_mode = []


    def disconnect(self):
        ModeSelectorComponent.disconnect(self)
        self._components_per_mode = None


    def add_mode(self, components, button):
        assert (components != None)
        assert isinstance(components, tuple)
        assert ((button == None) or isinstance(button, ButtonElement))
        if (len(self._modes_buttons) == 0):
            self._mode_index = 0
        for component in components:
            assert isinstance(component, ControlSurfaceComponent)
        if (button != None):
            identify_sender = True
            button.add_value_listener(self._mode_value, identify_sender)
            self._modes_buttons.append(button)
        self._components_per_mode.append(components)
        self.update()


    def number_of_modes(self):
        return len(self._components_per_mode)


    def update(self):
        assert ((len(self._modes_buttons) == 0) or (len(self._modes_buttons) == len(self._components_per_mode)))
        assert (len(self._components_per_mode) > self._mode_index)
        index = 0
        active_components = None
        if self.is_enabled():
            active_components = self._components_per_mode[self._mode_index]
        for index in range(len(self._components_per_mode)):
            if (self._components_per_mode[index] != active_components):
                if (len(self._modes_buttons) == len(self._components_per_mode)):
                    self._modes_buttons[index].turn_off()
                for component in self._components_per_mode[index]:
                    component.set_enabled(False)
        if (active_components != None):
            for component in active_components:
                component.set_enabled(True)
            if (len(self._modes_buttons) == len(self._components_per_mode)):
                self._modes_buttons[self._mode_index].turn_on()


# local variables:
# tab-width: 4
