# emacs-mode: -*- python-*-
# -*- coding: utf-8 -*-

from ControlSurfaceComponent import ControlSurfaceComponent 
class CompoundComponent(ControlSurfaceComponent):
    ' Base class for classes encompasing other components to form complex components '


    def __init__(self):
        ControlSurfaceComponent.__init__(self)
        self._sub_components = []


    def disconnect(self):
        self._sub_components = None


    def register_components(self, component):
        assert (component != None)
        assert isinstance(component, ControlSurfaceComponent)
        assert (list(self._sub_components).count(component) is 0)
        self._sub_components.append(component)
        component.set_enabled(self.is_enabled())


    def set_enabled(self, enable):
        assert isinstance(enable, (int,
         type(False)))
        bool_enable = (int(enable) != 0)
        if (self.is_enabled() != bool_enable):
            for component in self._sub_components:
                component.set_enabled(bool_enable)

            ControlSurfaceComponent.set_enabled(self, bool_enable)


    def set_allow_update(self, allow_updates):
        assert isinstance(allow_updates, (int,
         type(False)))
        allow = (int(allow_updates) != 0)
        if (self._allow_updates != allow):
            for component in self._sub_components:
                component.set_allow_update(allow)

            ControlSurfaceComponent.set_allow_update(self, allow)



# local variables:
# tab-width: 4
