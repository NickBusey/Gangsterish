# --== Decompile ==--

import Live
from NotifyingControlElement import NotifyingControlElement
from ControlElement import ControlElement
MIDI_NOTE_TYPE = 0
MIDI_CC_TYPE = 1
MIDI_PB_TYPE = 2
MIDI_MSG_TYPES = (MIDI_NOTE_TYPE, MIDI_CC_TYPE, MIDI_PB_TYPE)
MIDI_NOTE_ON_STATUS = 144
MIDI_NOTE_OFF_STATUS = 128
MIDI_CC_STATUS = 176
MIDI_PB_STATUS = 224

class InputControlElement(NotifyingControlElement):
    
    __doc__ = ' Base class for all classes representing control elements on a controller '
    _mapping_callback = None
    _forwarding_callback = None
    _translation_callback = None
    
    def set_mapping_callback(callback):
        """ set callback for installing mappings """
        assert (dir(callback).count('im_func') is 1)
        InputControlElement._mapping_callback = callback

    set_mapping_callback = staticmethod(set_mapping_callback)
    
    def set_forwarding_callback(callback):
        """ set callback for installing forwardings """
        assert (dir(callback).count('im_func') is 1)
        InputControlElement._forwarding_callback = callback

    set_forwarding_callback = staticmethod(set_forwarding_callback)
    
    def set_translation_callback(callback):
        """ set callback for installing translations """
        assert (dir(callback).count('im_func') is 1)
        InputControlElement._translation_callback = callback

    set_translation_callback = staticmethod(set_translation_callback)
    
    def release_class_attributes():
        """ release all set objects to not have dependencies """        
        InputControlElement._mapping_callback = None
        InputControlElement._forwarding_callback = None
        InputControlElement._translation_callback = None
        return None

    release_class_attributes = staticmethod(release_class_attributes)
    
    def __init__(self, msg_type, channel, identifier):
        assert (msg_type in MIDI_MSG_TYPES)
        assert (channel in range(16))
        assert ((identifier in range(128)) or (identifier is -1))
        NotifyingControlElement.__init__(self)
        assert (InputControlElement._mapping_callback != None)
        assert (InputControlElement._forwarding_callback != None)
        assert (InputControlElement._translation_callback != None)
        self._msg_type = msg_type
        self._msg_channel = channel
        self._msg_identifier = identifier
        self._original_channel = channel
        self._original_identifier = identifier
        self._needs_takeover = True
        self._is_mapped = True
        self._is_being_forwarded = True
        self._mapping_feedback_delay = 0
        self._parameter_to_map_to = None
        self._last_sent_value = -1
        self._install_mapping = InputControlElement._mapping_callback
        self._install_forwarding = InputControlElement._forwarding_callback
        self._install_translation = InputControlElement._translation_callback
        self._report_input = False
        self._report_output = False
        return None

    
    def disconnect(self):
        NotifyingControlElement.disconnect(self)
        self._parameter_to_map_to = None
        return None

    
    def message_type(self):
        return self._msg_type

    
    def message_channel(self):
        return self._msg_channel

    
    def message_identifier(self):
        return self._msg_identifier

    
    def message_map_mode(self):
        debug_print('message_map_mode() is abstract. Forgot to override it?')
        assert False
        

    def set_channel(self, channel):
        assert (channel in range(16))
        self._msg_channel = channel

        
    def set_identifier(self, identifier):
        assert ((identifier in range(128)) or (identifier is -1))
        self._msg_identifier = identifier

        
    def set_needs_takeover(self, needs_takeover):
        assert isinstance(needs_takeover, type(False))
        assert (self.message_type() != MIDI_NOTE_TYPE)
        self._needs_takeover = needs_takeover

        
    def set_feedback_delay(self, delay):
        assert (delay >= -1)
        self._mapping_feedback_delay = delay

        
    def needs_takeover(self):
        assert (self.message_type() != MIDI_NOTE_TYPE)
        return self._needs_takeover

    
    def use_default_message(self):
        self._msg_channel = self._original_channel
        self._msg_identifier = self._original_identifier

        
    def install_connections(self):
        self._is_mapped = False
        self._is_being_forwarded = False
        if ((self._msg_channel != self._original_channel) or (self._msg_identifier != self._original_identifier)):
            self._install_translation(self._msg_type, self._original_identifier, self._original_channel, self._msg_identifier, self._msg_channel)
        if (self._parameter_to_map_to != None):
            value_map = tuple()
            if (self._mapping_feedback_delay != 0):
                if (self._msg_type != MIDI_PB_TYPE):
                    value_map = tuple(range(128))
                else:
                    assert False
            self._is_mapped = self._install_mapping(self, self._parameter_to_map_to, self._mapping_feedback_delay, value_map)
        if ((len(self._value_notifications) > 0) or self._report_input):
            self._is_being_forwarded = self._install_forwarding(self)
        return None

    
    def connect_to(self, parameter):
        assert (parameter != None)
        assert isinstance(parameter, Live.DeviceParameter.DeviceParameter)
        self._parameter_to_map_to = parameter
        return None

    
    def release_parameter(self):
        self._parameter_to_map_to = None
        return None

    
    def mapped_parameter(self):
        return self._parameter_to_map_to

    
    def status_byte(self):
        status_byte = self._msg_channel
        if (self._msg_type == MIDI_NOTE_TYPE):
            status_byte += MIDI_NOTE_ON_STATUS
        elif (self._msg_type == MIDI_CC_TYPE):
            status_byte += MIDI_CC_STATUS
        else:
            status_byte += MIDI_PB_STATUS
        return status_byte
    

    def send_value(self, value, force_send = False):
        assert (value != None)
        assert isinstance(value, int)
        assert (value in range(128))
        if (force_send or ((value != self._last_sent_value) and self._is_being_forwarded)):
            data_byte1 = self._original_identifier
            data_byte2 = value
            status_byte = self._original_channel
            if (self._msg_type == MIDI_NOTE_TYPE):
                status_byte += MIDI_NOTE_ON_STATUS
            elif (self._msg_type == MIDI_CC_TYPE):
                status_byte += MIDI_CC_STATUS
            else:
                assert False
            if self.send_midi((status_byte, data_byte1, data_byte2)): #modified; added if
                self._last_sent_value = value
                if self._report_output:
                    is_input = True
                    self._report_value(value, (not is_input))
        
        
    def clear_send_cache(self):
        self._last_sent_value = -1

        
    def reset(self):
        """ Send 0 to reset motorized faders and turn off LEDs """
        self.send_value(0)

        
    def receive_value(self, value):
        assert isinstance(value, int)
        assert (value in range(128))
        self._last_sent_value = -1
        #self._last_received_value = value
        for notification in self._value_notifications:
            callback = notification['Callback']
            if notification['Identify']:
                callback(value, self)
            else:
                callback(value)

        if self._report_input:
            is_input = True
            self._report_value(value, is_input)

            
    def set_report_values(self, report_input, report_output):
        assert isinstance(report_input, type(False))
        assert isinstance(report_output, type(False))
        self._report_input = report_input
        self._report_output = report_output

        
    def _report_value(self, value, is_input):
        assert (value in range(128))
        assert isinstance(is_input, type(False))
        message = str(self.__class__.__name__) + ' ('
        if (self._msg_type == MIDI_NOTE_TYPE):
            message += (('Note ' + str(self._msg_identifier)) + ', ')
        elif (self._msg_type == MIDI_CC_TYPE):
            message += (('CC ' + str(self._msg_identifier)) + ', ')
        else:
            message += 'PB '
        message += 'Chan. ' + str(self._msg_channel)
        message += ') '
        if is_input:
            message += 'received value '
        else:
            message += 'sent value '
        message += str(value)
        debug_print(message)



