# emacs-mode: -*- python-*-
# -*- coding: utf-8 -*-

import Live 
from EncoderElement import EncoderElement 
from InputControlElement import * 
class SliderElement(EncoderElement):
    ' Class representing a slider on the controller '


    def __init__(self, msg_type, channel, identifier):
        assert (msg_type is not MIDI_NOTE_TYPE)
        EncoderElement.__init__(self, msg_type, channel, identifier, map_mode=Live.MidiMap.MapMode.absolute)



# local variables:
# tab-width: 4
