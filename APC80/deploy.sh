#!/bin/bash

if [ "$1" != '8' ] ; then
	if [ "$1" != '9' ] ; then
		echo "Specify 8 or 9"
		exit
	fi
fi

echo "Killing Live."
killall Live
echo "Deploying scripts."
if [ "$1" == '8' ] ; then
	cp -R Version\ 4/APC80/APC80_20 /Applications/Ableton\ Live\ 8.app/Contents/App-Resources/MIDI\ Remote\ Scripts/
	cp -R Version\ 4/APC80/APC80_40 /Applications/Ableton\ Live\ 8.app/Contents/App-Resources/MIDI\ Remote\ Scripts/
	open -a Ableton\ Live\ 8
	echo "Launching Live."
	sleep 5
	tail -f ~/Library/Preferences/Ableton/Live\ 8.4.2/Log.txt

elif  [ "$1" == '9' ] ; then
	cp -R APC80/Version\ 4.1/APC80_20 /Applications/Ableton\ Live\ 9\ Suite.app/Contents/App-Resources/MIDI\ Remote\ Scripts/
	cp -R APC80/Version\ 4.1/APC80_40 /Applications/Ableton\ Live\ 9\ Suite.app/Contents/App-Resources/MIDI\ Remote\ Scripts/
	cp -R APC40/Version\ 4.1/APC40_DJ /Applications/Ableton\ Live\ 9\ Suite.app/Contents/App-Resources/MIDI\ Remote\ Scripts/
	cp -R APC20/Version\ 4.1/APC20_DJ /Applications/Ableton\ Live\ 9\ Suite.app/Contents/App-Resources/MIDI\ Remote\ Scripts/

	# cp -R Version\ 4.1/APC80_20 /Applications/Ableton\ Live\ 9\ Suite.app/Contents/App-Resources/MIDI\ Remote\ Scripts/
	# cp -R Version\ 4.1/APC80_40 /Applications/Ableton\ Live\ 9\ Suite.app/Contents/App-Resources/MIDI\ Remote\ Scripts/

	open -a Ableton\ Live\ 9\ Suite
	echo "Launching Live."
	# sleep 5
	tail -f ~/Library/Preferences/Ableton/Live\ 9.1.4/Log.txt
fi
