import Live
from APC20_DJ import APC20_DJ

def create_instance(c_instance):
    """ Creates and returns the Gangsterish APC20_DJ script """
    return APC20_DJ(c_instance)

if Live.Application.get_application().get_major_version() == 9:
	from _Framework.Capabilities import *

def get_capabilities():
    return {CONTROLLER_ID_KEY: controller_id(vendor_id=2536, product_ids=[123], model_name='Akai APC20'),
     PORTS_KEY: [inport(props=[NOTES_CC, SCRIPT, REMOTE]), outport(props=[SCRIPT, REMOTE])]}