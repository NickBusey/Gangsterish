import Live 

IS_LIVE_9 = Live.Application.get_application().get_major_version() == 9
IS_LIVE_9_1 = IS_LIVE_9 and Live.Application.get_application().get_minor_version() >= 1
COLOR_OFF=0 # off - no clip
COLOR_GREEN=1 # playing
COLOR_GREEN_BLINK=2 # about to play (follow action)
COLOR_RED=3 # clip
COLOR_RED_BLINK=4 # clip played
COLOR_YELLOW=5 # track
COLOR_YELLOW_BLINK=6 # track played

MANUFACTURER_ID = 71
ABLETON_MODE = 65
NOTE_MODE = 67