#Embedded file name: /Users/versonator/Hudson/live/Projects/AppLive/Resources/MIDI Remote Scripts/_Framework/ButtonMatrixElement.py
from _Framework.CompoundElement import CompoundElement
from _Framework.Util import in_range, product, const

class GButtonMatrixElement(CompoundElement):
    """
    Class representing a 2-dimensional set of buttons.

    When using as a resource, buttons might be individually grabbed at
    any time by other components. The matrix will automatically block
    messages coming from or sent to a button owned by them, and will
    return None when you try to query it.
    """

    def __init__(self, *a, **k):
        super(GButtonMatrixElement, self).__init__(*a, **k)
        self._buttons = []
        self._button_coordinates = {}
        self._max_row_width = 0

    def add_row(self, buttons):
        self._buttons.append([None] * len(buttons))
        for index, button in enumerate(buttons):
            self._button_coordinates[button] = (index, len(self._buttons) - 1)
            self.register_control_element(button)

        if self._max_row_width < len(buttons):
            self._max_row_width = len(buttons)

    def width(self):
        return self._max_row_width

    def height(self):
        return len(self._buttons)

    def send_value(self, column, row, value, force = False):
        assert (value in range(128))
        assert (column in range(self.width()))
        assert (row in range(self.height()))
        if (len(self._buttons[row]) > column):
            self._buttons[row][column].send_value(value, force)

    def set_light(self, column, row, value):
        assert (in_range(column, 0, self.width()))
        assert (in_range(row, 0, self.height()))
        button = len(self._buttons[row]) > column and self._buttons[row][column]
        button and button.set_light(value)

    def get_button(self, column, row):
        assert (in_range(column, 0, self.width()))
        assert (in_range(row, 0, self.height()))
        return len(self._buttons[row]) > column and self._buttons[row][column]

    def reset(self):
        for button in self:
            if button:
                button.reset()

    def __iter__(self):
        for i, j in product(xrange(self.width()), xrange(self.height())):
            button = self.get_button(i, j)
            yield button

    def __getitem__(self, index):
        if isinstance(index, slice):
            indices = index.indices(len(self))
            return map(self._do_get_item, range(*indices))
        else:
            if index < 0:
                index += len(self)
            return self._do_get_item(index)

    def _do_get_item(self, index):
        assert (in_range(index, 0, len(self)))
        row, col = divmod(index, self.width())
        return self.get_button(col, row)

    def __len__(self):
        return self.width() * self.height()

    def iterbuttons(self):
        for i, j in product(xrange(self.width()), xrange(self.height())):
            button = self.get_button(i, j)
            yield (button, (i, j))

    def on_nested_control_element_value(self, value, sender):
        x, y = self._button_coordinates[sender]
        assert (self._buttons[y][x])
        is_momentary = getattr(sender, 'is_momentary', const(None))()
        self.notify_value(value, x, y, is_momentary)

    def on_nested_control_element_grabbed(self, control):
        x, y = self._button_coordinates[control]
        self._buttons[y][x] = control

    def on_nested_control_element_released(self, control):
        x, y = self._button_coordinates[control]
        self._buttons[y][x] = None