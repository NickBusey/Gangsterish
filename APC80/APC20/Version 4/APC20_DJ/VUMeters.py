import Live
from _Framework.ControlSurfaceComponent import ControlSurfaceComponent
from _Framework.ButtonElement import ButtonElement
from _Framework.SessionComponent import SessionComponent
import math

AUDIO_SOURCE = 1

LED_RED = 3
LED_ON = 127
LED_OFF = 0
LED_ORANGE = 5

# Scaling constants. Narrows the db range we display to ~0db-21db
CHANNEL_SCALE_MAX = 0.92
CHANNEL_SCALE_MIN = 0.52

MASTER_SCALE_MAX = 0.92
MASTER_SCALE_MIN = 0.52
MASTER_SCALE_INCREMENTS = 5

RMS_FRAMES = 2
USE_RMS = True

class VUMeter():
  'represents a single VU to store RMS values etc in'
  def __init__(self, parent, track, top, bottom,
              increments, master = False):

    self.frames = [0.0] * RMS_FRAMES
    self.parent = parent
    self.track = track
    self.top = top
    self.bottom = bottom
    self.multiplier = self.calculate_multiplier(top, bottom, increments)
    self.current_level = 0
    self.matrix = self.setup_matrix(master)
    self.master = master

  def observe(self):
    new_frame = self.mean_peak()
    self.store_frame(new_frame)
    if self.master and new_frame >= 0.92:
      self.parent._clipping = True
      self.parent.clip_warning()
    else:

      if self.master and self.parent._clipping:
        self.parent._parent._session._change_offsets(0, 1)
        self.parent._parent._session._change_offsets(0, -1)


        self.parent._clipping = False

      if not self.parent._clipping:
        if USE_RMS:
          level = self.scale(self.rms(self.frames))
        else:
          level = self.scale(new_frame)
        if level != self.current_level:
          self.current_level = level
          if self.master:
            self.parent.set_master_leds(level)
          else:
            self.parent.set_leds(self.matrix, level, self.master)

  def store_frame(self, frame):
    self.frames.pop(0)
    self.frames.append(frame)

  def rms(self, frames):
    return math.sqrt(sum(frame*frame for frame in frames)/len(frames))

  # return the mean of the L and R peak values
  def mean_peak(self):
    return (self.track.output_meter_left + self.track.output_meter_right) / 2


  # Perform the scaling as per params. We reduce the range, then round it out to integers
  def scale(self, value):
    if (value > self.top):
      value = self.top
    elif (value < self.bottom):
      value = self.bottom
    value = value - self.bottom
    value = value * self.multiplier #float, scale 0-10
    return int(round(value))

  def calculate_multiplier(self, top, bottom, increments):
    return (increments / (top - bottom))


  # Goes from top to bottom: so clip grid, then stop, then select, then activator/solo/arm
  def setup_matrix(self, master):
    matrix = []
    if master:
      for scene in self.parent._parent._session._scenes:
        matrix.append(scene._launch_button)
    else:
      for index in range(self.parent._width):
          if self.parent._trackB:
            index = index + self.parent._width
          strip = self.parent._parent._mixer.channel_strip(index)
          matrix.append(strip._mute_button)
    return matrix


class VUMeters(ControlSurfaceComponent):
    'standalone class used to handle VU meters'

    def __init__(self, parent, trackB = False, width = 8):
        # Boilerplate
        ControlSurfaceComponent.__init__(self)
        self._parent = parent

        # Default the audio and Master levels to 0
        self._meter_level = 0
        self._audio_level = 0
        self._trackB = trackB
        self._width = width

        # We don't start clipping
        self._clipping = False

        # The track we'll be pulling RMS from
        source = AUDIO_SOURCE
        if trackB:
          source = source + 1
        self._audio_track = self.song().tracks[source]

        self._parent.log_message("VU Meter Source: "+str(source)+str(trackB))

        #setup classes
        self.audio_meter = VUMeter(self, self._audio_track,
                                  CHANNEL_SCALE_MAX,
                                  CHANNEL_SCALE_MIN, width)

        self.master_meter = VUMeter(self, self.song().master_track,
                                    MASTER_SCALE_MAX,
                                    MASTER_SCALE_MIN, MASTER_SCALE_INCREMENTS,
                                    True)
        # Listeners!
        self._audio_track.add_output_meter_left_listener(self.audio_meter.observe)
        self.song().master_track.add_output_meter_left_listener(self.master_meter.observe)

    # If you fail to kill the listeners on shutdown, Ableton stores them in memory and punches you in the face
    def disconnect(self):
        self._audio_track.remove_output_meter_left_listener(self.audio_meter.observe)
        self.song().master_track.remove_output_meter_left_listener(self.master_meter.observe)

    # Called when the Master clips. Makes the entire clip grid BRIGHT RED
    def clip_warning(self):
      for row_index in range(CLIP_GRID_Y):
        row = self._parent._button_rows[row_index]
        for button_index in range(CLIP_GRID_X):
          button = row[button_index]
          # Passing True to send_value forces it to happen even when the button in question is MIDI mapped
          button.send_value(LED_RED, True)

    def set_master_leds(self, level):
        for scene_index in range(4):
            scene = self._parent._session.scene(scene_index)
            if scene_index >= (4 - level):
              scene._launch_button.send_value(LED_ON, True)
            else:
              scene._launch_button.send_value(LED_OFF, True)

    def set_leds(self, matrix, level, master):
      if master:
        for index in range(5):
          button = matrix[index]
          if index >= (5 - level):
            button.send_value(LED_ON, True)
          else:
            button.send_value(LED_OFF, True)
      else:
        if self._trackB:
          ii = 0;
          for index in reversed(range(self._width)):
            button = matrix[index]
            if ii >= (self._width - level):
                button.send_value(LED_ON, True)
            else:
              button.send_value(LED_OFF, True)
            ii = ii+1
        else:
          for index in range(self._width):
            button = matrix[index]
            if index >= (self._width - level):
              button.send_value(LED_ON, True)
            else:
              button.send_value(LED_OFF, True)

    def update(self):
        pass

    def on_enabled_changed(self):
        self.update()

    def on_selected_track_changed(self):
        self.update()

    def on_track_list_changed(self):
        self.update()

    def on_selected_scene_changed(self):
        self.update()

    def on_scene_list_changed(self):
        self.update()