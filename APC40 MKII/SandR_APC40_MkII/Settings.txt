###################################################################
#
#   Sends & Returns APC40MK II Template Settings
#
#   2015 http://sendsandreturns.com
#
#	Percentages are set from 0 to 1, so use .25 for 25% percent
#
####################################################################

# Scroll by page (like when you scroll with shift pressed normally) rather than by individual scene
	# Default: TRUE
	# Options: TRUE or FALSE
PAGE_SCROLL=TRUE

# Number of tracks wide to use. 4 if fitting a beat jumper, 8 for all tracks
	# Default: 8
	# Options: 4 or 8
NUM_TRACKS=8

# Percentage clip played that will flash it yellow, indicating the clip is about to end.
	# Default: .75
	# Options: .01 (1%) - .99 (99%)
SONG_ENDING_SOON=.75

# Percentage clip played that will flash it red, and mark it as played in memory.
	# Default: .90
	# Options: .01 (1%) - .99 (99%)
SONG_ENDING_NOW=.90

# When to mark clip as played
	# Default: .50 (50%)
	# Options: .01 (1%) - .99 (99%)
CLIP_IS_PLAYED=.50

# Whether or not to mark clips as played at all
	# Default TRUE
	# Options: TRUE or FALSE
MARK_CLIPS_AS_PLAYED=TRUE

# Whether or not the Activator (Mute) buttons should be momentary
	# Default: TRUE
	# Options: TRUE or FALSE
MOMENTARY_MUTE_BUTTONS=TRUE

# Whether or not the Solo buttons should be momentary
	# Default: TRUE
	# Options: TRUE or FALSE
MOMENTARY_SOLO_BUTTONS=TRUE

# APC40 Width (you can decrease this to assign buttons to other things such as ClyphX controls)
	# Default: 8
	# Options: 1 - 8
APC40_WIDTH=8

# APC40 Height
	# Default: 5
	# Options: 1 - 5
APC40_HEIGHT=5

# APC20 Width
	# Default: 8
	# Options: 1 - 8
APC20_WIDTH=8

# APC40 Height
	# Default: 5
	# Options: 1 - 5
APC20_HEIGHT=5

# BPM Setting s
	# First number is how many beats to transition over
	# Second number is target BPM
	# Default: 8 85
	# Options: 1-128 (beats) 20-999 (BPM)
BPM_1 = 8 85
BPM_2 = 8 90
BPM_3 = 8 95
BPM_4 = 8 100
BPM_5 = 8 110
BPM_6 = 8 120
BPM_7 = 8 130
BPM_8 = 8 140

# Default BPM
	# Which BPM setting above to start the template in.
	# Default: 5
	# Options: 1-8
DEFAULT_BPM=5

