from _Framework.CompoundComponent import CompoundComponent 
from _Framework.ButtonElement import ButtonElement 
from _Framework.MixerComponent import MixerComponent 
from _Framework.ButtonMatrixElement import ButtonMatrixElement
from _Framework.ControlSurface import ControlSurface

class GLooperComponent(CompoundComponent):
    ' SelectorComponent that controls loops '
    __module__ = __name__

    def __init__(self, parent, track_index, loop_button, clear_button, half_button, double_button):
        CompoundComponent.__init__(self)
        self._parent = parent
        self._started_loop = False
        self._is_looping = False
        self._loop_button = loop_button
        self._clear_button = clear_button
        self._half_button = half_button
        self._double_button = double_button
        self._track_index = track_index

        loop_button.add_value_listener(self._loop_value, True)
        clear_button.add_value_listener(self._clear_value, True)
        half_button.add_value_listener(self._half_value, True)
        double_button.add_value_listener(self._double_value, True)
        
    def disconnect(self):
        for button in self._modes_buttons:
            button.remove_value_listener(self._mode_value)
        self._controls = None
        self._session = None
        self._session_zoom = None
        self._buttons = None
        self._button_matrix = None
        ModeSelectorComponent.disconnect(self)

    def _loop_value(self, value, sender):
        track = self._parent.song().tracks[self._track_index]
        if value > 0:
            if self._started_loop == False:
                self._started_loop = True
                clip = self._get_clip()
                if clip:
                    self._parent.log_message("Got clip: ")
                    self._parent.log_message(clip)
                    loop_start = round(clip.playing_position)
                    self._parent.log_message("Loop it"+str(loop_start))
                    clip.looping = 1
                    self._set_loop_start(clip,loop_start)
                    clip.looping = 0
            else:
                self._started_loop = False
                self._is_looping = True
                clip = self._get_clip()
                if clip:
                        loop_end = round(clip.playing_position)
                        self._parent.log_message("End loop"+str(loop_end))
                        clip.looping = 1
                        clip.loop_end = loop_end

    def _set_loop_start(self, clip, loop_start):
        try:
            clip.loop_end = loop_start + 1
        except:
            pass
        try:
            clip.loop_start = loop_start
        except:
            pass
        try:
            clip.loop_end = loop_start + 1
        except:
            pass

    def _get_clip(self):
        track = self._parent.song().tracks[self._track_index]
        offset = self._parent._session._scene_offset
        self._parent.log_message("Got scene offset: "+str(offset))
        index = track.playing_slot_index
        if track.clip_slots[index].has_clip and track.clip_slots[index].clip.is_playing:
            return track.clip_slots[index].clip
        return False


    def _clear_value(self, value, sender):
        if value > 0:
            self._parent.log_message("Clear it")
            clip = self._get_clip()
            if clip:
                clip.looping = 0
            self._started_loop = False
            self._is_looping = False

    def _half_value(self, value, sender):
        if value > 0 and self._is_looping:
            self._parent.log_message("Halfies")
            clip = self._get_clip()
            if clip:
                length = clip.loop_end - clip.loop_start
                clip.loop_start = clip.loop_start + (length / 2)

    def _double_value(self, value, sender):
        if value > 0 and self._is_looping:
            self._parent.log_message("Dubs")
            clip = self._get_clip()
            if clip:
                length = clip.loop_end - clip.loop_start
                clip.loop_start = clip.loop_start - length