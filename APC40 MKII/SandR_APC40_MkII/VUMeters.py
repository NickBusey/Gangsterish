import Live
from _Framework.ControlSurfaceComponent import ControlSurfaceComponent
from _Framework.ButtonElement import ButtonElement
from _Framework.SessionComponent import SessionComponent
from _Framework.ButtonElement import Color
from Push.Colors import Rgb, Pulse, Blink
import math

AUDIO_SOURCE = 1

LED_RED = 3
LED_ON = 127
LED_OFF = 0
LED_ORANGE = 5

# Scaling constants. Narrows the db range we display to ~0db-21db
CHANNEL_SCALE_MAX = 0.92
CHANNEL_SCALE_MIN = 0.52
CHANNEL_SCALE_INCREMENTS = 4

MASTER_SCALE_MAX = 0.92
MASTER_SCALE_MIN = 0.52
MASTER_SCALE_INCREMENTS = 5

RMS_FRAMES = 2
USE_RMS = True

GREEN = Color(1)
GREEN_BLINK = Color(2)
RED = Color(3)
RED_BLINK = Color(4)
AMBER = Color(5)

class VUMeter():
  'represents a single VU to store RMS values etc in'
  def __init__(self, parent, track, top, bottom,
              increments, master = False):

    self.frames = [0.0] * RMS_FRAMES
    self.parent = parent
    self.track = track
    self.top = top
    self.bottom = bottom
    self.multiplier = self.calculate_multiplier(top, bottom, increments)
    self.current_level = 0
    self.matrix = self.setup_matrix(master)
    self.master = master

  def observe(self):
    new_frame = self.mean_peak()
    self.store_frame(new_frame)
    if self.master and new_frame >= 0.92:
      self.parent._clipping = True
      self.parent.clip_warning()
    else:

      if self.master and self.parent._clipping:
        self.parent._parent._session._change_offsets(0, 1)
        self.parent._parent._session._change_offsets(0, -1)


        self.parent._clipping = False

      if not self.parent._clipping:
        if USE_RMS:
          level = self.scale(self.rms(self.frames))
        else:
          level = self.scale(new_frame)
        if level != self.current_level:
          self.current_level = level
          if self.master:
            self.parent.set_master_leds(level)
          else:
            self.parent.set_leds(self.matrix, level, self.master)

  def store_frame(self, frame):
    self.frames.pop(0)
    self.frames.append(frame)

  def rms(self, frames):
    return math.sqrt(sum(frame*frame for frame in frames)/len(frames))

  # return the mean of the L and R peak values
  def mean_peak(self):
    return (self.track.output_meter_left + self.track.output_meter_right) / 2


  # Perform the scaling as per params. We reduce the range, then round it out to integers
  def scale(self, value):
    if (value > self.top):
      value = self.top
    elif (value < self.bottom):
      value = self.bottom
    value = value - self.bottom
    value = value * self.multiplier #float, scale 0-10
    return int(round(value))

  def calculate_multiplier(self, top, bottom, increments):
    return (increments / (top - bottom))


  # Goes from top to bottom: so clip grid, then stop, then select, then activator/solo/arm
  def setup_matrix(self, master):
    return self.parent._parent._scene_launch_buttons
    

class VUMeters(ControlSurfaceComponent):
    'standalone class used to handle VU meters'

    def __init__(self, parent):
        # Boilerplate
        ControlSurfaceComponent.__init__(self)
        self._parent = parent

        # Default the audio and Master levels to 0
        self._meter_level = 0
        self._audio_level = 0

        # We don't start clipping
        self._clipping = False

        self.master_meter = VUMeter(self, self.song().master_track,
                                    MASTER_SCALE_MAX,
                                    MASTER_SCALE_MIN, MASTER_SCALE_INCREMENTS,
                                    True)
        # Listeners!
        self.song().master_track.add_output_meter_left_listener(self.master_meter.observe)

    # If you fail to kill the listeners on shutdown, Ableton stores them in memory and punches you in the face
    def disconnect(self):
        self._audio_track.remove_output_meter_left_listener(self.audio_meter.observe)
        self.song().master_track.remove_output_meter_left_listener(self.master_meter.observe)

    # Called when the Master clips. Makes the entire clip grid BRIGHT RED
    def clip_warning(self):
      for scene_index in range(5):
          scene = self._parent._session.scene(scene_index)
          scene._launch_button.send_value(Rgb.RED, True)

    def set_master_leds(self, level):
        colors = [Rgb.RED, Rgb.AMBER, Rgb.GREEN, Rgb.GREEN, Rgb.GREEN]
        for scene_index in range(5):
            scene = self._parent._session.scene(scene_index)
            if scene_index >= (5 - level):
              scene._launch_button.send_value(colors[scene_index], True)
            else:
              scene._launch_button.send_value(LED_OFF, True)

    def set_leds(self, matrix, level, master):
      if master:
        for index in range(5):
          button = matrix[index]
          if index >= (5 - level):
            button.send_value(LED_ON, True)
          else:
            button.send_value(LED_OFF, True)
      else:
        if self._trackB:
          ii = 0;
          for index in reversed(range(4)):
            button = matrix[index]
            if ii >= (4 - level):
                button.send_value(LED_ON, True)
            else:
              button.send_value(LED_OFF, True)
            ii = ii+1
        else:
          for index in range(4):
            button = matrix[index]
            if index >= (4 - level):
              button.send_value(LED_ON, True)
            else:
              button.send_value(LED_OFF, True)

    def update(self):
        pass

    def on_enabled_changed(self):
        self.update()

    def on_selected_track_changed(self):
        self.update()

    def on_track_list_changed(self):
        self.update()

    def on_selected_scene_changed(self):
        self.update()

    def on_scene_list_changed(self):
        self.update()