#!/bin/bash

echo "Killing Live."
killall Live
echo "Deploying scripts."
	cp -R SandR_APC40_MkII /Applications/Ableton\ Live\ 9\ Suite.app/Contents/App-Resources/MIDI\ Remote\ Scripts/
	open -a Ableton\ Live\ 9\ Suite
	echo "Launching Live."
	tail -f ~/Library/Preferences/Ableton/Live\ 9.2/Log.txt

