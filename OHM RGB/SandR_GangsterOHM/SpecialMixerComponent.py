from _Framework.MixerComponent import MixerComponent

class SpecialMixerComponent(MixerComponent):
	' Special mixer class that uses return tracks alongside midi and audio tracks'
	__module__ = __name__


	def __init__(self, *a, **k):
		self._is_locked = False #added
		super(SpecialMixerComponent, self).__init__(*a, **k)
	

	def on_selected_track_changed(self):
		selected_track = self.song().view.selected_track
		if selected_track != None:
			if (self._selected_strip != None):
				if self._is_locked == False: #added
					self._selected_strip.set_track(selected_track)
			if self.is_enabled():
				if (self._next_track_button != None):
					if (selected_track != self.song().master_track):
						self._next_track_button.turn_on()
					else:
						self._next_track_button.turn_off()
				if (self._prev_track_button != None):
					if (selected_track != self.song().tracks[0]):
						self._prev_track_button.turn_on()
					else:
						self._prev_track_button.turn_off()		  
	

	def tracks_to_use(self):
		return tuple(self.song().visible_tracks) + tuple(self.song().return_tracks)
	