from _Framework.ButtonElement import ButtonElement
from _Framework.ModeSelectorComponent import ModeSelectorComponent
from _Mono_Framework.MonoButtonElement import MonoButtonElement
from _Mono_Framework.MonomodComponent import MonomodComponent
from _Framework.ButtonMatrixElement import ButtonMatrixElement

class GangsterOHMMonomodComponent(MonomodComponent):
	__module__ = __name__
	__doc__ = ' Component that encompasses and controls 4 Monomod clients '


	def __init__(self, script, *a, **k):
		super(GangsterOHMMonomodComponent, self).__init__(script, *a, **k)
		self._host_name = 'Cntrlr'
	

	def disconnect(self):
		#self._script.log_message('monomod disconnect')
		self.set_allow_update(False)  ###added
		self._active_client = None
		self._set_shift_button(None)
		self._set_lock_button(None)
		self._set_nav_buttons(None)
		self._set_key_buttons(None)
#		self._set_dial_matrix(None, None)
		self._set_button_matrix(None)
		self._client = []
		self._script = []
		return None 
	

	def connect_to_clients(self, monomod):
		self._client = monomod._client
		self._select_client(0)
		#self._active_client._is_active = True
		#self._script.log_message('connected to clients')
	

	def _select_client(self, number):
		self._active_client = self._client[number]
		self._colors = self._color_maps[number]
		for client in self._client:
			if self in client._active_host:
				client._active_host.remove(self)
		self._active_client._active_host.append(self)
		self._x = self._offsets[number][0]
		self._y = self._offsets[number][1]
		self._script.set_local_ring_control(self._active_client._local_ring_control)
		self._script.schedule_message(5, self._script.set_absolute_mode, self._active_client._absolute_mode)
		self._active_client._device_component.set_enabled(self._active_client._device_component._type != None)
		#self._active_client.set_channel()
		self.update()
	

	def _set_button_matrix(self, grid):
		assert isinstance(grid, (ButtonMatrixElement, type(None)))
		if grid != self._grid:
			if self._grid != None:
				self._grid.remove_value_listener(self._matrix_value)
			self._grid = grid
			if self._grid != None:
				self._grid.add_value_listener(self._matrix_value)
			for client in self._client:
				client._update_controls_dictionary()
			self.update()
		return None
	

	def _matrix_value(self, value, x, y, is_momentary):
		assert (self._grid != None)
		assert (value in range(128))
		assert isinstance(is_momentary, type(False))
		if (self.is_enabled()):
			self._active_client._send_grid(x + self._x, y + self._y, value)
	

	def _send_grid(self, column, row, value):
		if self.is_enabled() and self._grid != None:
			if column in range(self._x, self._x + 4):
				if row in range(self._y, self._y + 4):
					self._grid.get_button(column - self._x, row - self._y).send_value(int(self._colors[value]))

	

	def _alt_value(self, value):
		if self._shift_pressed == 0:
			self._alt_pressed = value != 0
			self._active_client._send('alt', int(self._alt_pressed))
			self.update()
	

	def _update_alt_button(self):
		if self._alt_button!=None:
			if self._alt_pressed != 0:
				self._alt_button.turn_on()
			else:
				self._alt_button.turn_off()
	

	def _set_key_buttons(self, buttons, *a, **k):
		assert (buttons == None) or (isinstance(buttons, tuple))
		for key in self._keys:
			if key.value_has_listener(self._key_value):
				key.remove_value_listener(self._key_value)
		self._keys = []
		if buttons != None:
			assert len(buttons) == 32
			for button in buttons:
				#assert isinstance(button, MonoButtonElement)
				self._keys.append(button)
				button.add_value_listener(self._key_value, True)
		for client in self._client:
			client._update_controls_dictionary()
	

	def _key_value(self, value, sender):
		if self.is_enabled():
			self._active_client._send_key(self._keys.index(sender), int(value!=0))
	

	def _update_keys(self):
		for index in range(32):
			self._send_key(index, self._active_client._key[index])
	

	def _send_key(self, index, value):
		if self.is_enabled():
			#if (self._shift_pressed > 0) or (self._locked > 0):
			#	self._grid.get_button(index, 7).send_value(int(self._colors[value]))
			if  self._keys != None and len(self._keys) > index:
				self._keys[index].send_value(int(self._colors[value]))
	

	def _set_knobs(self, knobs):
		assert (knobs == None) or (isinstance(knobs, tuple))
		for knob in self._knobs:
			knob.remove_value_listener(self._knob_value)
		self._knobs = []
		if knobs != None:
			assert len(knobs) == 24
			for knob in knobs:
				assert isinstance(knob, EncoderElement)
				self._knobs.append(knob)
				knob.add_value_listener(self._knob_value, True)
	

	def _knob_value(self, value, sender):
		if self.is_enabled():
			self._active_client._send_knob(self._knobs.index(sender), value)
	

	def on_enabled_changed(self):
		self._scroll_up_ticks_delay = -1
		self._scroll_down_ticks_delay = -1
		self._scroll_right_ticks_delay = -1
		self._scroll_left_ticks_delay = -1
		if self.is_enabled():
			self._active_client._device_component.set_enabled(self._active_client._device_component._type!=None)
			self._script.set_absolute_mode(self._active_client._absolute_mode)
			self._script.set_local_ring_control(self._active_client._local_ring_control)
		else:
			self._active_client._device_component.set_enabled(False)
			self._script.set_absolute_mode(1)
			self._script.set_local_ring_control(1)
		self.update()
	

	def _set_dial_matrix(self, dial_matrix, button_matrix):
		assert isinstance(dial_matrix, (EncoderMatrixElement, type(None)))
		if dial_matrix != self._dial_matrix:
			if self._dial_matrix != None:
				self._dial_matrix.remove_value_listener(self._dial_matrix_value)
			self._dial_matrix = dial_matrix
			if self._dial_matrix != None:
				self._dial_matrix.add_value_listener(self._dial_matrix_value)
			
		assert isinstance(button_matrix, (ButtonMatrixElement, type(None)))
		if button_matrix != self._dial_button_matrix:
			if self._dial_button_matrix != None:
				self._dial_button_matrix.remove_value_listener(self._dial_button_matrix_value)
			self._dial_button_matrix = button_matrix
			if self._dial_button_matrix != None:
				self._dial_button_matrix.add_value_listener(self._dial_button_matrix_value)
		for client in self._client:
			client._update_controls_dictionary()
		self.update()
		return None
	

	def _dial_matrix_value(self, value, x, y):
		if self.is_enabled() and self._active_client != None:
			if self._script._absolute_mode == 0:
				value = RELATIVE[int(value == 1)]
			self._active_client._send_dial(x, y, value)
	

	def _reset_encoder(self, coord):
		self._dial_matrix.get_dial(coord[0], coord[1])._reset_to_center()
	

	def _dial_button_matrix_value(self, value, x, y, force):
		if (self.is_enabled()) and (self._active_client != None):
			self._active_client._send_dial_button(x, y, value)
	

	def _send_wheel(self, column, row, wheel):
		if self.is_enabled() and wheel != None:  ##not isinstance(wheel, type(None)):
				if column < 4 and row < 3:
					dial = self._dial_matrix.get_dial(column, row)
					dial._ring_value = int(wheel['value'])
					dial._ring_mode = int(wheel['mode'])
					dial._ring_green = int(wheel['green']!=0)
					dial._ring_log = int(wheel['log'])
					#if dial._raw_custom != str(wheel['custom']):
					dial._ring_custom = dial._calculate_custom(str(wheel['custom']))	##comon, really?  Everytime??
				self._dial_button_matrix.send_value(column, row, wheel['white'])
				if(self._script._absolute_mode > 0) and (not self._active_client._device_component.is_enabled()):
					dial.send_value(wheel['log'], True)
				#elif(self._device.is_enabled()):
				#	self._dial_matrix.get_dial(column, row).set_value(wheel['value'])
					
				##Need to insert routine for sending to MonoDevice from here, so that parameters can be updated from it.

	

	def _send_to_lcd(self, column, row, wheel):
		#self._script.log_message('send lcd ' + str(column) + ' ' + str(row) + ' ' + str(wheel['pn']))
		if self.is_enabled() and not self._active_client._device_component.is_enabled():
			self._script.notification_to_bridge(str(wheel['pn']), str(wheel['pv']), self._dial_matrix.get_dial(column, row))
	

	def _update_wheel(self):
		if self._dial_button_matrix != None:
			for column in range(4):
				for row in range(3):
					self._send_wheel(column, row, self._active_client._wheel[column][row])
					if not self._active_client._device_component.is_enabled():
						self._send_to_lcd(column, row, self._active_client._wheel[column][row])
						#self._script.log_message('dial value update' +str(column) + str(row) + str(self._active_client._wheel[column][row]['value']))
	