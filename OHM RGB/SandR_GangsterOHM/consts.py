import Live 

#Colors = [white, yellow, cyan, magenta, red, green, blue]
COLOR_MAP = [2, 64, 4, 8, 16, 127, 32]

#	In addition, there are two further color maps that are used depending on whether the RGB or Monochrome 
#		Ohm64 is detected.  The second number is the color used by the RGB (from the ordering in the COLOR_MAP array),
#		the first the Monochrome.  Obviously the Monochrome doesn't use the colors.  
#	However, the flashing status of a color is derived at by modulus.  Thus 1-7 are the raw colors, 8-15 are a fast
#		flashing color, 15-22 flash a little slower, etc.  127 is always solid.  You can assign your own color maps here:



IS_LIVE_9 = Live.Application.get_application().get_major_version() == 9
IS_LIVE_9_1 = IS_LIVE_9 and Live.Application.get_application().get_minor_version() >= 1
COLOR_OFF=0 # off - no clip
COLOR_GREEN=6
COLOR_GREEN_BLINK=13
COLOR_RED=5
COLOR_RED_BLINK=12
COLOR_YELLOW=2
COLOR_YELLOW_BLINK=9
