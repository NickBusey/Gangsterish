from _Framework.CompoundComponent import CompoundComponent 
from _Framework.ButtonElement import ButtonElement 
from _Framework.MixerComponent import MixerComponent 
from _Framework.ButtonMatrixElement import ButtonMatrixElement
from _Framework.ControlSurface import ControlSurface
from Push.Colors import RgbColor, Rgb, Pulse, Blink


class GBeatJumper(CompoundComponent):
    ' SelectorComponent that controls loops '
    __module__ = __name__

    def __init__(self, parent, beat_buttons, track_start):
        CompoundComponent.__init__(self, beat_buttons)
        self._parent = parent
        self._beat_buttons = beat_buttons
        self._columns = [0,1]
        self._current_bar = 0
        self._current_beat = 0
        self._track_start = track_start
        # Goes [Y][X] for some dumb reason
        beat_buttons[0][0].add_value_listener(self._beat_1_1, True)
        beat_buttons[1][0].add_value_listener(self._beat_1_2, True)
        beat_buttons[2][0].add_value_listener(self._beat_1_3, True)
        beat_buttons[3][0].add_value_listener(self._beat_1_4, True)

        beat_buttons[0][1].add_value_listener(self._beat_2_1, True)
        beat_buttons[1][1].add_value_listener(self._beat_2_2, True)
        beat_buttons[2][1].add_value_listener(self._beat_2_3, True)
        beat_buttons[3][1].add_value_listener(self._beat_2_4, True)

        # Setup quick movers
        beat_buttons[4][0].add_value_listener(self._go_back, True)
        beat_buttons[4][1].add_value_listener(self._go_forward, True)
        
    def disconnect(self):
        for button in self._modes_buttons:
            button.remove_value_listener(self._mode_value)
        self._controls = None
        self._session = None
        self._session_zoom = None
        self._buttons = None
        self._button_matrix = None
        ModeSelectorComponent.disconnect(self)

    def tick(self):
        for column in self._columns:
            for button in self._beat_buttons:
                button[column].send_value(False, True)

        pos = self._playing_pos()
        if pos:
            beat = int(pos % 4)
            bar = int(pos / 4)
            self._current_beat = beat
            self._current_bar = bar
            if bar % 2 == 0:
                column = 0
                last_column = 1
            else:
                column = 1
                last_column = 0

            # Mod to 57 because that's how many colors the MKII has according to Colors.py
            pos_color = pos % 57
            self._beat_buttons[beat][column].send_value(RgbColor(int(pos_color)), True)

    def _go_to(self, bar, beat):
        playing_clip = self._get_playing_clip()
        beat_jump = beat-(self._current_beat+1)
        if bar % 2 == 1 and self._current_bar % 2 == 0:
            beat_jump += 4
        elif bar % 2 == 0 and self._current_bar % 2 == 1:
            beat_jump -= 4

        playing_clip.move_playing_pos(beat_jump)

    def _go_back(self, value, sender):
        playing_clip = self._get_playing_clip()
        playing_clip.move_playing_pos(-4)

    def _go_forward(self, value, sender):
        playing_clip = self._get_playing_clip()
        playing_clip.move_playing_pos(4)

    # Buncha bullshit because I didn't feel like fucking with the value_listeners and don't know any better.
    def _beat_1_1(self, value, sender):
        if value:
            self._go_to(0,0)

    def _beat_1_2(self, value, sender):
        if value:
            self._go_to(0,1)

    def _beat_1_3(self, value, sender):
        if value:
            self._go_to(0,2)

    def _beat_1_4(self, value, sender):
        if value:
            self._go_to(0,3)

    def _beat_2_1(self, value, sender):
        if value:
            self._go_to(1,0)

    def _beat_2_2(self, value, sender):
        if value:
            self._go_to(1,1)

    def _beat_2_3(self, value, sender):
        if value:
            self._go_to(1,2)

    def _beat_2_4(self, value, sender):
        if value:
            self._go_to(1,3)

    def _get_playing_clip(self):
        slot_index = self._parent.song().tracks[self._track_start].playing_slot_index
        playing_clip = self._parent.song().tracks[self._track_start].clip_slots[slot_index].clip
        if playing_clip == None:
            slot_index = self._parent.song().tracks[self._track_start+1].playing_slot_index
            playing_clip = self._parent.song().tracks[self._track_start+1].clip_slots[slot_index].clip
        return playing_clip

    def _playing_pos(self):
        playing_clip = self._get_playing_clip()
        if playing_clip:
            playing_pos = playing_clip.playing_position
            return playing_pos
