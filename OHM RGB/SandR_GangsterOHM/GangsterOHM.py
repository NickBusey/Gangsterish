# amounra 0513 : http://www.aumhaa.com

from __future__ import with_statement
import Live
import time
import math
from _Framework.ButtonElement import ButtonElement
from _Framework.ButtonMatrixElement import ButtonMatrixElement
from _Framework.ChannelStripComponent import ChannelStripComponent
from _Framework.ClipSlotComponent import ClipSlotComponent
from _Framework.CompoundComponent import CompoundComponent
from _Framework.ControlElement import ControlElement
from _Framework.ControlSurface import ControlSurface
from _Framework.ControlSurfaceComponent import ControlSurfaceComponent
from _Framework.DeviceComponent import DeviceComponent
from _Framework.SessionComponent import SessionComponent
from _Framework.DisplayDataSource import DisplayDataSource
from _Framework.EncoderElement import EncoderElement
from _Framework.InputControlElement import *
from VCM600.MixerComponent import MixerComponent
from _Framework.ModeSelectorComponent import ModeSelectorComponent
from _Framework.NotifyingControlElement import NotifyingControlElement
from _Framework.SceneComponent import SceneComponent
# from _Framework.SessionZoomingComponent import DeprecatedSessionZoomingComponent as SessionZoomingComponent
from _Framework.SliderElement import SliderElement
from VCM600.TrackFilterComponent import TrackFilterComponent
from _Framework.TransportComponent import TransportComponent
from _Framework.M4LInterfaceComponent import M4LInterfaceComponent

from _Mono_Framework.MonomodComponent import MonomodComponent
from _Mono_Framework.MonoBridgeElement import MonoBridgeElement
from _Mono_Framework.MonoEncoderElement import MonoEncoderElement
from _Mono_Framework.MonoBridgeElement import MonoBridgeElement
from _Mono_Framework.DetailViewControllerComponent import DetailViewControllerComponent
from _Mono_Framework.MonoButtonElement import MonoButtonElement
from _Mono_Framework.SwitchboardElement import SwitchboardElement
from _Mono_Framework.MonoClient import MonoClient
from _Mono_Framework.CodecEncoderElement import CodecEncoderElement
from _Mono_Framework.EncoderMatrixElement import EncoderMatrixElement
from _Mono_Framework.LiveUtils import *
from _Mono_Framework.ModDevices import *
from _Mono_Framework.Debug import *

from MonoDeviceComponent import MonoDeviceComponent

from _Generic.Devices import *
from Map import *

from ShiftModeComponent import ShiftModeComponent
from ModNumModeComponent import ModNumModeComponent
from OctaveModeComponent import OctaveModeComponent
from GangsterOHMMonoClient import GangsterOHMMonoClient
from ScaleModeComponent import ScaleModeComponent
# from SpecialMixerComponent import SpecialMixerComponent

from GangsterOHMMonomodComponent import GangsterOHMMonomodComponent
from GSessionComponent import GSessionComponent
from GBPMComponent import GBPMComponent
from GSimpleVU import GSimpleVU
from GMomentaryButtonElement import GMomentaryButtonElement
from GSessionZoomingComponent import GSessionZoomingComponent

FOLDER = '/SandR_GangsterOHM/'

session = None
mixer = None
switchxfader = (240, 0, 1, 97, 2, 15, 1, 247)
check_model = (240, 126, 127, 6, 1, 247)
KEYS = [[4, 0],
 [4, 1],
 [4, 2],
 [4, 3],
 [4, 4],
 [4, 5],
 [4, 6],
 [4, 7],
 [6, 0],
 [6, 1],
 [6, 2],
 [6, 3],
 [6, 4],
 [6, 5],
 [6, 6],
 [6, 7],
 [5, 0],
 [5, 1],
 [5, 2],
 [5, 3],
 [5, 4],
 [5, 5],
 [5, 6],
 [5, 7],
 [7, 0],
 [7, 1],
 [7, 2],
 [7, 3],
 [7, 4],
 [7, 5],
 [7, 6],
 [7, 7]]
TEMPO_TOP = 200.0
TEMPO_BOTTOM = 60.0
MIDI_NOTE_TYPE = 0
MIDI_CC_TYPE = 1
MIDI_PB_TYPE = 2
MIDI_MSG_TYPES = (MIDI_NOTE_TYPE, MIDI_CC_TYPE, MIDI_PB_TYPE)
MIDI_NOTE_ON_STATUS = 144
MIDI_NOTE_OFF_STATUS = 128
MIDI_CC_STATUS = 176
MIDI_PB_STATUS = 224

INC_DEC = [-1, 1]

class SpecialMixerComponent(MixerComponent):
	' Special mixer class that uses return tracks alongside midi and audio tracks'
	__module__ = __name__


	def __init__(self, *a, **k):
		self._is_locked = False #added
		super(SpecialMixerComponent, self).__init__(*a, **k)
	

	def on_selected_track_changed(self):
		selected_track = self.song().view.selected_track
		if selected_track != None:
			if (self._selected_strip != None):
				if self._is_locked == False: #added
					self._selected_strip.set_track(selected_track)
			if self.is_enabled():
				if (self._next_track_button != None):
					if (selected_track != self.song().master_track):
						self._next_track_button.turn_on()
					else:
						self._next_track_button.turn_off()
				if (self._prev_track_button != None):
					if (selected_track != self.song().tracks[0]):
						self._prev_track_button.turn_on()
					else:
						self._prev_track_button.turn_off()		  

	def tracks_to_use(self):
		return tuple(self.song().visible_tracks) + tuple(self.song().return_tracks)

class GangsterOHM(ControlSurface):
	__module__ = __name__
	__doc__ = ' GangsterOHM controller script '

	def __init__(self, c_instance):
		super(GangsterOHM, self).__init__(c_instance)

		self._version_check = 'b994'
		self._host_name = 'Ohm'
		self._color_type = 'OhmRGB'
		self._hosts = []
		self.hosts = []
		self._client = [ None for index in range(6) ]
		self._active_client = None
		self._rgb = 0
		self._timer = 0
		self._touched = 0
		self.flash_status = 1
		self._backlight = 127
		self._backlight_type = 'static'
		self._ohm = 127
		self._ohm_type = 'static'
		self._pad_translations = PAD_TRANSLATION
		self._device_selection_follows_track_selection = FOLLOW
		self._keys_octave = 5
		self._keys_scale = 0
		self._tempo_buttons = None
		self._num_scenes = 5
		self._num_tracks = 8

		if self.song().tracks[0].name != 'Gangsterish APC':
			return

		with self.component_guard():
			self._setup_monobridge()
			self._setup_controls()
			self._setup_m4l_interface()
			self._setup_transport_control()
			self._setup_mixer_control()
			self._setup_session_control()
			self._setup_device_control()
			self._setup_crossfader()
			self._setup_ohmmod()
			self._setup_switchboard()
			self._setup_modes()
			self._assign_page_constants()
			self._last_device = None
			self.song().view.add_selected_track_listener(self._update_selected_device)
			self.show_message('GangsterOHM Control Surface Loaded')
			self._send_midi(tuple(switchxfader))
		if FORCE_TYPE is True:
			self._rgb = FORCE_COLOR_TYPE
		else:
			self.schedule_message(10, self.query_ohm, None)
		self.log_message('<<<<<<<<<<<<<<<<<<<<<<<<< GangsterOHM ' + str(self._version_check) + ' log opened >>>>>>>>>>>>>>>>>>>>>>>>>')
	

		# Gangsterish
		self.song().add_current_song_time_listener(self.on_time_changed)
		self.song().add_is_playing_listener(self.on_time_changed)
		self._current_bar = 1
		self._current_beat = 1
		self._current_quarter_beat = 1
		self._current_eight_beat = 1
		self._metronome = 1
		self._last_beat = -1
		self._tempo = -1
		self._tempo_ramp_active = False
		self._tempo_ramp_settings = []
		self._has_cleared_fx = -1
		self._mic_flash = 0
		self._slower = False
		self._faster = False

	def query_ohm(self):
		self._send_midi(tuple(check_model))
	

	def update_display(self):
		ControlSurface.update_display(self)
		self._timer = (self._timer + 1) % 256
		self.flash()
		self.strobe()
	

	def _setup_monobridge(self):
		self._monobridge = MonoBridgeElement(self)
		self._monobridge.name = 'MonoBridge'
	

	def get_device_bank(self):
		return self._device._bank_index
	

	def _setup_controls(self):
		is_momentary = True
		self._fader = [ None for index in range(8) ]
		self._dial = [ None for index in range(16) ]
		self._button = [ None for index in range(8) ]
		self._menu = [ None for index in range(6) ]
		for index in range(8):
			self._fader[index] = MonoEncoderElement(MIDI_CC_TYPE, CHANNEL, OHM_FADERS[index], Live.MidiMap.MapMode.absolute, 'Fader_' + str(index), index, self)

		for index in range(8):
			self._button[index] = GMomentaryButtonElement(self, MIDI_NOTE_TYPE, CHANNEL, OHM_BUTTONS[index])

		for index in range(16):
			self._dial[index] = CodecEncoderElement(MIDI_CC_TYPE, CHANNEL, OHM_DIALS[index], Live.MidiMap.MapMode.absolute, 'Encoder_' + str(index), index, self)

		self._knobs = []
		for index in range(12):
			self._knobs.append(self._dial[index])

		for index in range(6):
			self._menu[index] = MonoButtonElement(is_momentary, MIDI_NOTE_TYPE, CHANNEL, OHM_MENU[index], 'Menu_' + str(index), self)

		self._crossfader = EncoderElement(MIDI_CC_TYPE, CHANNEL, CROSSFADER, Live.MidiMap.MapMode.absolute)
		self._crossfader.name = 'Crossfader'
		self._livid = MonoButtonElement(is_momentary, MIDI_NOTE_TYPE, CHANNEL, LIVID, 'Livid_Button', self)
		self._shift_l = GMomentaryButtonElement(self, MIDI_NOTE_TYPE, CHANNEL, SHIFT_L)
		self._shift_r = GMomentaryButtonElement(self, MIDI_NOTE_TYPE, CHANNEL, SHIFT_R)
		self._matrix = ButtonMatrixElement()
		self._matrix.name = 'Matrix'
		self._grid = [ None for index in range(8) ]
		self._monomod = ButtonMatrixElement()
		self._monomod.name = 'Monomod'
		for column in range(8):
			self._grid[column] = [ None for index in range(8) ]
			for row in range(self._num_scenes):
				self._grid[column][row] = MonoButtonElement(is_momentary, MIDI_NOTE_TYPE, CHANNEL, column * 8 + row, 'Grid_' + str(column) + '_' + str(row), self)

		for row in range(self._num_scenes):
			button_row = []
			for column in range(self._num_tracks):
				button_row.append(self._grid[column][row])

			self._matrix.add_row(tuple(button_row))

		for row in range(self._num_scenes):
			button_row = []
			for column in range(8):
				button_row.append(self._grid[column][row])

			self._monomod.add_row(tuple(button_row))

		self._mod_matrix = ButtonMatrixElement()
		self._mod_matrix.name = 'Matrix'
		self._dial_matrix = EncoderMatrixElement(self)
		self._dial_matrix.name = 'Dial_Matrix'
		self._dial_button_matrix = ButtonMatrixElement()
		self._dial_button_matrix.name = 'Dial_Button_Matrix'
		for row in range(4):
			button_row = []
			for column in range(4):
				button_row.append(self._grid[column + 4][row])

			self._mod_matrix.add_row(tuple(button_row))

		for row in range(3):
			dial_row = []
			for column in range(4):
				dial_row.append(self._dial[row * 4 + column])

			self._dial_matrix.add_row(tuple(dial_row))

		for row in range(3):
			dial_button_row = []
			for column in range(4):
				dial_button_row.append(self._grid[column][row])

			self._dial_button_matrix.add_row(tuple(dial_button_row))

		self._key = [ self._grid[KEYS[index][1]][KEYS[index][0]] for index in range(32) ]
		self._encoder = [ self._dial[index] for index in range(12) ]
	

	def _setup_m4l_interface(self):
		self._m4l_interface = M4LInterfaceComponent(controls=self.controls, component_guard=self.component_guard)
		self.get_control_names = self._m4l_interface.get_control_names
		self.get_control = self._m4l_interface.get_control
		self.grab_control = self._m4l_interface.grab_control
		self.release_control = self._m4l_interface.release_control
	

	def _setup_ohmmod(self):
		self._host = GangsterOHMMonomodComponent(self)
		self._host.name = 'Monomod_Host'
		self.hosts = [self._host]
		self._hosts = [self._host]
		for index in range(6):
			self._client[index] = GangsterOHMMonoClient(self, index)
			self._client[index].name = 'Client_' + str(index)
			self._client[index]._device_component = MonoDeviceComponent(self._client[index], MOD_BANK_DICT, MOD_TYPES)
			#self._client[index]._device_component.set_parameter_controls(tuple([ self._dial[num] for num in range(12) ]))
			self._client[index]._control_defs = {'dials': self._dial_matrix,
			 'buttons': self._dial_button_matrix,
			 'grid': self._mod_matrix,
			 'keys': self._key,
			 'knobs': [ self._dial[num + 12] for num in range(4) ]}
		self._host._set_parameter_controls(self._dial)
		self._host._active_client = self._client[0]
		self._host._active_client._is_active = True
		self._host.connect_to_clients(self)
	

	def _setup_switchboard(self):
		self._switchboard = SwitchboardElement(self, self._client)
		self._switchboard.name = 'Switchboard'
	

	def _setup_modes(self):
		self._shift_mode = ShiftModeComponent(self)
		self._shift_mode.name = 'Shift_Mode'
		self._shift_mode.set_mode_toggle(self._shift_l, self._shift_r, None)
		self._scale_mode = ScaleModeComponent(self)
		self._scale_mode.name = 'Scale_Mode'
		self._octave_mode = OctaveModeComponent(self)
		self._octave_mode.name = 'Octave_Mode'
		self._modNum = ModNumModeComponent(self, self.modNum_update)
		self._modNum.name = 'Mod_Number'
		self._modNum.set_mode = self._modNum_set_mode(self._modNum)
		self._modNum.set_mode_buttons(self._menu)
		self.log_message("Setup modes!!")

	def _modNum_set_mode(self, modNum):
		def set_mode(mode):
			if modNum._is_enabled == True:
				assert isinstance(mode, int)
				assert (mode in range(modNum.number_of_modes()))
				if (modNum._mode_index != mode):
					modNum._mode_index = mode
					modNum.update()
		return set_mode

	def _setup_transport_control(self):
		self._transport = TransportComponent()
		self._transport.name = 'Transport'
	
	def _setup_mixer_control(self):
		global mixer
		is_momentary = True
		mixer = SpecialMixerComponent(self._num_tracks, 0, True, False)
		mixer.name = 'Mixer'
		self._mixer = mixer
		for index in range(self._num_tracks):
			mixer.channel_strip(index).set_volume_control(self._fader[index])

		for index in range(self._num_tracks):
			mixer.channel_strip(index).name = 'Mixer_ChannelStrip_' + str(index)
			mixer.track_eq(index).name = 'Mixer_EQ_' + str(index)
			mixer.channel_strip(index)._invert_mute_feedback = True

		self.song().view.selected_track = mixer.channel_strip(0)._track
	
		#Gangsterish
		self._master_mixer = self._mixer.master_strip()._track.mixer_device
		self._bpm = str(self._master_mixer.song_tempo).split('.')[0]
		
		self._bpm_buttons = [ None for index in range(8) ]
		for column in range(8):
			self._bpm_buttons[column] = MonoButtonElement(is_momentary, MIDI_NOTE_TYPE, CHANNEL, column * 8 + 7, 'Grid_' + str(column) + '_' + str(7), self)

		bpm_control = GBPMComponent(tuple(self._bpm_buttons), self)
		bpm_control.set_mode_buttons(tuple(self._bpm_buttons))
		self._bpm_control = bpm_control

		self._metronome_buttons = [ None for index in range(8) ]
		for column in range(8):
			self._metronome_buttons[column] = MonoButtonElement(is_momentary, MIDI_NOTE_TYPE, CHANNEL, column * 8 + 6, 'Grid_' + str(column) + '_' + str(6), self)
		
		self._stop_buttons = [ None for index in range(8) ]

		gohm = self

		def _stop_track(value, self):
			gohm.song().tracks[self.column+4].stop_all_clips()

		for column in range(8):
			button = MonoButtonElement(is_momentary, MIDI_NOTE_TYPE, CHANNEL, column * 8 + 5, 'Grid_' + str(column) + '_' + str(5), self)
			button.column = column
			button.add_value_listener(_stop_track, True)
			self._stop_buttons[column] = button
		
		vu0 = GSimpleVU(self,4,4)
		vu1 = GSimpleVU(self,5,4)
		vu2 = GSimpleVU(self,6,4)
		vu3 = GSimpleVU(self,7,4)
		vu4 = GSimpleVU(self,8,4)
		vu5 = GSimpleVU(self,9,4)
		vu6 = GSimpleVU(self,10,4)
		vu7 = GSimpleVU(self,11,4)

		self.song().start_playing()

	def _setup_session_control(self):
		self.get_user_settings()
		global session
		is_momentary = True
		session = GSessionComponent(self, self._num_tracks, self._num_scenes)
		# session = SessionComponent(self._num_tracks, self._num_scenes)
		session.name = 'Session'
		self._session = session
		session.set_offsets(1, 0)
		self._scene = [ None for index in range(self._num_scenes) ]
		for row in range(self._num_scenes):
			self._scene[row] = session.scene(row)
			self._scene[row].name = 'Scene_' + str(row)
			for column in range(self._num_tracks):
				clip_slot = self._scene[row].clip_slot(column)
				clip_slot.name = str(column) + '_Clip_Slot_' + str(row)

		session.set_mixer(self._mixer)
		session.set_show_highlight(True)

		self._session_zoom = GSessionZoomingComponent(self, 1)
		self._session_zoom.name = 'Session_Overview'
		self._session_zoom.set_button_matrix(self._matrix)
		self._session_zoom.set_zoom_button(self._livid)
		self._session_zoom.set_stopped_value(3)
		self._session_zoom.set_selected_value(5)

		self.set_highlighting_session_component(self._session)
	

	def _assign_session_colors(self):
		self.log_message('assign session colors')
		self._session.set_stop_clip_value(STOP_CLIP_COLOR[self._rgb])
		for row in range(self._num_scenes):
			for column in range(self._num_tracks):
				# clip = self._scene[row].clip_slot(column).clip
				# self.log_message("Got clip: "+clip)
				self._scene[row].clip_slot(column).set_triggered_to_play_value(CLIP_TRIGD_TO_PLAY_COLOR[self._rgb])
				self._scene[row].clip_slot(column).set_triggered_to_record_value(CLIP_TRIGD_TO_RECORD_COLOR[self._rgb])
				self._scene[row].clip_slot(column).set_stopped_value(CLIP_STOPPED_COLOR[self._rgb])
				self._scene[row].clip_slot(column).set_started_value(CLIP_STARTED_COLOR[self._rgb])
				self._scene[row].clip_slot(column).set_recording_value(CLIP_RECORDING_COLOR[self._rgb])

		self._session_zoom.set_stopped_value(ZOOM_STOPPED_COLOR[self._rgb])
		self._session_zoom.set_playing_value(ZOOM_PLAYING_COLOR[self._rgb])
		self._session_zoom.set_selected_value(ZOOM_SELECTED_COLOR[self._rgb])
		# for row in range(8):
		# 	for column in range(self._num_scenes):
		# 		self._grid[column][row].set_force_next_value()

		self._session.on_scene_list_changed()
		self._shift_mode.update()
	

	def _setup_device_control(self):
		self._device = DeviceComponent()
		self._device.name = 'Device_Component'
		self.set_device_component(self._device)
		self._device_navigator = DetailViewControllerComponent()
		self._device_navigator.name = 'Device_Navigator'
		self._device_selection_follows_track_selection = FOLLOW
	

	def device_follows_track(self, val):
		self._device_selection_follows_track_selection = val == 1
		return self
	

	def _setup_crossfader(self):
		self._mixer.set_crossfader_control(self._crossfader)
	

	def disconnect(self):
		"""clean things up on disconnect"""
		self.song().view.remove_selected_track_listener(self._update_selected_device)
		self.log_message(time.strftime('%d.%m.%Y %H:%M:%S', time.localtime()) + '--------------= GangsterOHM log closed =--------------')
		super(GangsterOHM, self).disconnect()
		rebuild_sys()
	

	def _get_num_tracks(self):
		return self.num_tracks
	

	def flash(self):
		if(self.flash_status > 0):
			for control in self.controls:
				if isinstance(control, MonoButtonElement):
					control.flash(self._timer)
	

	def strobe(self):
		if self._backlight_type != 'static':
			if self._backlight_type is 'pulse':
				self._backlight = int(math.fabs(self._timer * 16 % 64 - 32) + 32)
			if self._backlight_type is 'up':
				self._backlight = int(self._timer * 8 % 64 + 16)
			if self._backlight_type is 'down':
				self._backlight = int(math.fabs(int(self._timer * 8 % 64 - 64)) + 16)
		self._send_midi(tuple([176, 27, int(self._backlight)]))
		if self._ohm_type != 'static':
			if self._ohm_type is 'pulse':
				self._ohm = int(math.fabs(self._timer * 16 % 64 - 32) + 32)
			if self._ohm_type is 'up':
				self._ohm = int(self._timer * 8 % 64 + 16)
			if self._ohm_type is 'down':
				self._ohm = int(math.fabs(int(self._timer * 8 % 64 - 64)) + 16)
		self._send_midi(tuple([176, 63, int(self._ohm)]))
		self._send_midi(tuple([176, 31, int(self._ohm)]))
	

	def deassign_matrix(self):
		with self.component_guard():
			self._host._set_knobs(None)
			self._host._set_button_matrix(None)
			self._host.set_enabled(False)
			self._modNum.set_enabled(False)
			self.assign_alternate_mappings(0)
			self._scale_mode.set_mode_buttons(None)
			self._scale_mode.set_enabled(False)
			self._octave_mode.set_mode_buttons(None)
			self._octave_mode.set_enabled(False)
			self._session_zoom.set_enabled(False)
			self._session_zoom.set_nav_buttons(None, None, None, None)
			self._session.set_track_bank_buttons(None, None)
			self._session.set_scene_bank_buttons(None, None)
			self._transport.set_enabled(False)
			for column in range(4):
				self._mixer.track_eq(column)._gain_controls = None
				self._mixer.track_eq(column).set_enabled(False)

			for column in range(self._num_tracks):
				self._mixer.channel_strip(column).set_crossfade_toggle(None)
				self._mixer.channel_strip(column).set_mute_button(None)
				self._mixer.channel_strip(column).set_solo_button(None)
				self._mixer.channel_strip(column).set_arm_button(None)
				self._mixer.channel_strip(column).set_send_controls(None)
				self._mixer.channel_strip(column).set_pan_control(None)
				self._mixer.track_eq(column).set_enabled(False)
				for row in range(self._num_scenes):
					self._scene[row].clip_slot(column).set_launch_button(None)

			"""for column in range(8):
				self._button[column]._on_value = SELECT_COLOR[self._rgb]
				for row in range(8):
					self._grid[column][row].set_enabled(True)
					self._grid[column][row].release_parameter()
					self._grid[column][row].use_default_message()
					self._grid[column][row].set_on_off_values(127, 0)
					self._grid[column][row].send_value(0, True)"""

			for column in range(self._num_tracks):
				self._button[column]._on_value = SELECT_COLOR[self._rgb]
				for row in range(self._num_scenes):
					#self._grid[column][row].set_channel(0)
					self._grid[column][row].release_parameter()
					self._grid[column][row].use_default_message()
					self._grid[column][row].set_enabled(True)
					self._grid[column][row].send_value(0, True)
					self._grid[column][row]._on_value = 127
					self._grid[column][row]._off_value = 0
					self._grid[column][row].force_next_send()


			for index in range(6):
				self._menu[index]._on_value = 127
				self._menu[index]._off_value = 0

			for index in range(16):
				self._dial[index].use_default_message()
				self._dial[index].release_parameter()

			self._device.set_parameter_controls(None)
			self._device.set_enabled(False)
			self._device_navigator.set_enabled(False)
			self._mixer.update()
			self._matrix.reset()
		self.request_rebuild_midi_map()

	def _assign_page_constants(self):
		with self.component_guard():
			self._session_zoom.set_zoom_button(self._livid)
			self._session_zoom.set_button_matrix(self._matrix)

			self._mixer.set_prehear_volume_control(self._dial[15])
	
	# Clip mode
	def assign_page_0(self):
		with self.component_guard():
			self._backlight_type = 'up'
			self._session_zoom.set_enabled(True)

			for column in range(self._num_tracks):
				for row in range(self._num_scenes):
					self._scene[row].clip_slot(column).set_launch_button(self._grid[column][row])

			for index in range(4):
				self._menu[2 + index]._on_value = NAV_BUTTON_COLOR[self._rgb]

			self._session.set_page_down_button(self._menu[3])
			self._session.set_page_up_button(self._menu[0])
			self._session_zoom.set_nav_buttons(self._menu[0], self._menu[3], None, None)

			self._transport.set_enabled(True)
			#self._mixer.update()
		self.request_rebuild_midi_map()
	
	# Drum rack / sample page
	def assign_page_1(self):
		with self.component_guard():
			self._session_zoom.set_enabled(False)
			for column in range(4):
				for row in range(5):
					self._grid[column][row].send_value(DRUM_COLOR[self._rgb], True)
					self._grid[column + 4][row].send_value(BASS_COLOR[self._rgb], True)
					self._grid[column][row].set_enabled(False)
					self._grid[column][row]._msg_channel = PAGE1_DRUM_CHANNEL
					self._grid[column][row].set_identifier(PAGE1_DRUM_MAP[column][row])
					self._grid[column + 4][row].set_enabled(False)
					self._grid[column + 4][row]._msg_channel = PAGE1_BASS_CHANNEL
					self._grid[column + 4][row].set_identifier(PAGE1_DRUM_MAP[column][row])

		self.request_rebuild_midi_map()

	def assign_page_2(self):
		with self.component_guard():
			self._session_zoom.set_enabled(False)
			for column in range(4):
				for row in range(5):
					self._grid[column][row].send_value(DRUM_COLOR[self._rgb], True)
					self._grid[column + 4][row].send_value(BASS_COLOR[self._rgb], True)
					self._grid[column][row].set_enabled(False)
					self._grid[column][row]._msg_channel = PAGE1_DRUM_CHANNEL+2
					self._grid[column][row].set_identifier(PAGE1_DRUM_MAP[column][row])
					self._grid[column + 4][row].set_enabled(False)
					self._grid[column + 4][row]._msg_channel = PAGE1_BASS_CHANNEL+2
					self._grid[column + 4][row].set_identifier(PAGE1_DRUM_MAP[column][row])


		# def make_matrix_button(track, scene):
		# 	return MonoButtonElement(True, MIDI_NOTE_TYPE, CHANNEL, track * 8 + scene, 'Grid_' + str(track) + '_' + str(scene), self)

		# self.log_message("Show beat jumper!")
		# with self.component_guard():
		# 	self.deassign_matrix()
		# 	self._session_zoom.set_enabled(False)

		# 	self._beat_buttons = [ [ make_matrix_button(track, scene) for track in xrange(4,6) ] for scene in xrange(5) ]
		# 	self._beat_jumper = GBeatJumper(self, self._beat_buttons, 4)
		

		# 	# for column in range(4):
		# 	# 	for row in range(5):
		# 	# 		self._grid[column][row].send_value(DRUM_COLOR[self._rgb], True)
		# 	# 		self._grid[column + 4][row].send_value(BASS_COLOR[self._rgb], True)
		# 	# 		self._grid[column][row].set_enabled(False)
		# 	# 		self._grid[column][row]._msg_channel = PAGE1_DRUM_CHANNEL
		# 	# 		self._grid[column][row].set_identifier(PAGE1_DRUM_MAP[column][row])
		# 	# 		self._grid[column + 4][row].set_enabled(False)
		# 	# 		self._grid[column + 4][row]._msg_channel = PAGE1_BASS_CHANNEL
		# 	# 		self._grid[column + 4][row].set_identifier(PAGE1_DRUM_MAP[column][row])

		self.request_rebuild_midi_map()


	def assign_mod(self):
		with self.component_guard():
			self.deassign_matrix()
			self._host.set_enabled(True)
			self._modNum.set_enabled(True)
			self._host._set_dial_matrix(self._dial_matrix, self._dial_button_matrix)
			self._host._set_button_matrix(self._mod_matrix)
			self._host._set_key_buttons(tuple(self._key))
			if not self._host._active_client.is_connected():
				self.assign_alternate_mappings(self._modNum._mode_index + 1)
	

	def modNum_update(self):
		if self._modNum._is_enabled == True:
			self.assign_alternate_mappings(0)
			self._host._select_client(int(self._modNum._mode_index))
			self._host.display_active_client()
			if not self._host._active_client.is_connected():
				self.assign_alternate_mappings(self._modNum._mode_index + 1)
			for button in self._modNum._modes_buttons:
				if self._modNum._mode_index == self._modNum._modes_buttons.index(button):
					button.send_value(1)
				else:
					button.send_value(self._client[self._modNum._modes_buttons.index(button)]._mod_color)
	

	def assign_alternate_mappings(self, chan):
		for column in range(8):
			for row in range(self._num_scenes):
				self._grid[column][row].set_channel(chan)
		for knob in self._encoder:
			knob.set_channel(chan)
			knob.set_enabled(chan is 0)

		self.request_rebuild_midi_map()
	

	def display_mod_colors(self):
		pass
	

	def _update_selected_device(self):
		if self._device_selection_follows_track_selection is True:
			track = self.song().view.selected_track
			device_to_select = track.view.selected_device
			if device_to_select == None and len(track.devices) > 0:
				device_to_select = track.devices[0]
			if device_to_select != None:
				self.song().view.select_device(device_to_select)
			self.set_appointed_device(device_to_select)
			self.request_rebuild_midi_map()
	

	def handle_sysex(self, midi_bytes):
		#self.log_message('sysex: ' + str(midi_bytes))
		if len(midi_bytes) > 10:
			if midi_bytes[:11] == tuple([240,
			 126,
			 0,
			 6,
			 2,
			 0,
			 1,
			 97,
			 1,
			 0,
			 7]):
				self.log_message(str('>>>color detected'))
				self._rgb = 0
				for button in self._button:
					if button:
						button._color_map = COLOR_MAP
				for column in self._grid:
					for button in column:
						if button:
							button._color_map = COLOR_MAP
			elif midi_bytes[:11] == tuple([240,
			 126,
			 0,
			 6,
			 2,
			 0,
			 1,
			 97,
			 1,
			 0,
			 2]):
				self.log_message(str('>>>mono detected'))
				self._rgb = 1
				for button in self._button:
					button._color_map = [127 for index in range(0, 7)]
				for column in self._grid:
					for button in column:
						button._color_map = [127 for index in range(0, 7)]
		self._assign_session_colors()
	

	def to_encoder(self, num, val):
		rv = int(val * 127)
		self._device._parameter_controls[num].receive_value(rv)
		p = self._device._parameter_controls[num]._parameter_to_map_to
		newval = val * (p.max - p.min) + p.min
		p.value = newval
	

	def set_local_ring_control(self, val = 1):
		self._local_ring_control = val != 0
	

	def set_absolute_mode(self, val = 1):
		self._absolute_mode = val != 0
	

	def send_ring_leds(self):
		pass
	

	def _set_tempo_buttons(self, buttons):
		if self._tempo_buttons != None:
			self._tempo_buttons[0].remove_value_listener(self._tempo_value)
			self._tempo_buttons[1].remove_value_listener(self._tempo_value)
		self._tempo_buttons = buttons
		if buttons != None:
			for button in buttons:
				 assert isinstance(button, MonoButtonElement)
			self._tempo_buttons[0].set_on_off_values(4, 0)
			self._tempo_buttons[0].add_value_listener(self._tempo_value, True)
			self._tempo_buttons[1].set_on_off_values(4, 0)
			self._tempo_buttons[1].add_value_listener(self._tempo_value, True)
			self._tempo_buttons[0].turn_on()
			self._tempo_buttons[1].turn_on()
	

	def _tempo_value(self, value, sender):
		if value > 0 and self._tempo_buttons.index(sender) == 0:
			self.song().tempo = round(min(self.song().tempo + 1, 999))
		elif value > 0 and self._tempo_buttons.index(sender) == 1:
			self.song().tempo = round(max(self.song().tempo - 1, 20))
	

	def generate_strip_string(self, display_string):
		NUM_CHARS_PER_DISPLAY_STRIP = 12
		if not display_string:
			return ' ' * NUM_CHARS_PER_DISPLAY_STRIP
		if len(display_string.strip()) > NUM_CHARS_PER_DISPLAY_STRIP - 1 and display_string.endswith('dB') and display_string.find('.') != -1:
			display_string = display_string[:-2]
		if len(display_string) > NUM_CHARS_PER_DISPLAY_STRIP - 1:
			for um in [' ',
			 'i',
			 'o',
			 'u',
			 'e',
			 'a']:
				while len(display_string) > NUM_CHARS_PER_DISPLAY_STRIP - 1 and display_string.rfind(um, 1) != -1:
					um_pos = display_string.rfind(um, 1)
					display_string = display_string[:um_pos] + display_string[um_pos + 1:]

		else:
			display_string = display_string.center(NUM_CHARS_PER_DISPLAY_STRIP - 1)
		ret = u''
		for i in range(NUM_CHARS_PER_DISPLAY_STRIP - 1):
			if ord(display_string[i]) > 127 or ord(display_string[i]) < 0:
				ret += ' '
			else:
				ret += display_string[i]

		ret += ' '
		return ret
	

	def notification_to_bridge(self, name, value, sender):
		if isinstance(sender, tuple([MonoButtonElement, CodecEncoderElement])):
			self._monobridge._send(sender.name, 'lcd_name', str(self.generate_strip_string(name)))
			self._monobridge._send(sender.name, 'lcd_value', str(self.generate_strip_string(value)))
	

	def touched(self):
		if self._touched is 0:
			self._monobridge._send('touch', 'on')
			self.schedule_message(2, self.check_touch)
		self._touched += 1
	

	def check_touch(self):
		if self._touched > 5:
			self._touched = 5
		elif self._touched > 0:
			self._touched -= 1
		if self._touched is 0:
			self._monobridge._send('touch', 'off')
		else:
			self.schedule_message(2, self.check_touch)
	

	def get_clip_names(self):
		clip_names = []
		for scene in self._session._scenes:
			for clip_slot in scene._clip_slots:
				if clip_slot.has_clip() is True:
					clip_names.append(clip_slot._clip_slot)
					return clip_slot._clip_slot

		return clip_names
	

	def shift_update(self):
		pass
	
	def get_user_settings(self):
		if not hasattr(self, "_user_variables"):
			self._user_variables = {}
		script_path = ''
		for path in sys.path:
			if 'MIDI Remote Scripts' in path:
				script_path = path
				break
		user_file = script_path + FOLDER + 'Settings.txt'
		for line in open(user_file): 
			line = line.rstrip('\n')
			if not line.startswith(('#', '"',)) and not line == '' and '=' in line:
				var_data = line.split('=')
				if len(var_data) >= 2 and not ';' in var_data[1] and not '%' in var_data[1] and not '=' in var_data[1]:
					self._user_variables[var_data[0].strip()] = var_data[1].strip()

	def on_time_changed(self):
		bar = int(str(self.song().get_current_beats_song_time()).split('.')[0])
		beat = int(str(self.song().get_current_beats_song_time()).split('.')[1])
		quarter_beat = int(str(self.song().get_current_beats_song_time()).split('.')[2])
		eight_beat = int(str(self.song().get_current_beats_song_time()).split('.')[3])
		if bar != self._current_bar:
			self._slower = False
			self._faster = False
			self._current_bar = bar
			foundBPM = False
			for scene_index in range(self._num_scenes):
				scene = self._session.scene(scene_index)
				for track_index in range(self._num_tracks):
					clip_slot = scene.clip_slot(track_index)
					if (clip_slot._clip_slot and clip_slot._clip_slot.clip):
						bpm = clip_slot._clip_slot.clip.name.split(' ')[0]
						try:
							track_bpm = float("%.2f" % float(bpm))
						except:
							break
						foundBPM = True
						break
				if foundBPM:
					break
			if foundBPM:
				song_bpm = self.song().tempo
				if self._tempo_ramp_active:
					song_bpm = self._tempo_ramp_settings[0]
				if track_bpm < song_bpm:
					self._faster = True
				if track_bpm > song_bpm:
					self._slower = True
			# self.log_message("Bar: "+str(bar))
			self._knobs[2].send_value(0, True)

		if beat != self._current_beat:
			# self.log_message("Current beat: "+str(beat))
			# self.log_message("Current bar: "+str(bar))
			beatOfEight = beat
			if (bar % 2):
				beatOfEight += 4
			self._current_beat = beat
			self._ofEight = ofEight = beatOfEight - 1 # Turn 1-8 into 0-7

			self._bpm_control.update_bpm()

			self._metronome_buttons[ofEight].send_value(127, True)
			self._metronome_buttons[ofEight - 1].send_value(0, True)


			if self._has_cleared_fx == -1 and self.song().tracks[0].name=='Gangsterish APC':
				self.song().scenes[5].set_fire_button_state(1);
				self._has_cleared_fx = 0
				self._bpm_control.set_mode(int(self._user_variables['DEFAULT_BPM'])-1)
			elif self._has_cleared_fx == 0:
				self.song().scenes[5].set_fire_button_state(0);
				self._bpm_control.update_bpm()
				self._has_cleared_fx = 1
				for btn in self._stop_buttons:
					btn.send_value(0, True)
				for btn in self._metronome_buttons:
					btn.send_value(0, True)
				for btn in self._bpm_buttons:
					btn.send_value(0, True)

		if quarter_beat != self._current_quarter_beat:
			self._current_quarter_beat = quarter_beat
			if self._tempo_ramp_active and self._tempo_ramp_settings and self.song().is_playing:
				self._tasks.add(self.apply_tempo_ramp)
		# 	if self.song().tracks[3].mute == False:
		# 		if self._mic_flash == 1:
		# 			for btn in self._mute_buttons:
		# 				btn.send_value(127, True)
		# 			for btn in self._solo_buttons:
		# 				btn.send_value(127, True)
		# 			for btn in self._arm_buttons:
		# 				btn.send_value(127, True)
		# 			self._mic_flash = 0
		# 		else:
		# 			for btn in self._mute_buttons:
		# 				btn.send_value(0, True)
		# 			for btn in self._solo_buttons:
		# 				btn.send_value(0, True)
		# 			for btn in self._arm_buttons:
		# 				btn.send_value(0, True)
		# 			self._mic_flash = 1

			ebeatOfEight = quarter_beat -1
			if (beat % 2):
				ebeatOfEight += 4
			self._session.recolor_buttons();

		if eight_beat != self._current_eight_beat:
			self._current_eight_beat = eight_beat

	def adjust_tempo(self, args):
		self._tempo_ramp_active = False
		self._tempo_ramp_settings = []
		arg_array = args.split()
		if len(arg_array) == 2:
			try:
				ramp_factor = float("%.2f" % (int(arg_array[0]) * self.song().signature_numerator))
				target_tempo = float("%.2f" % float(arg_array[1]))
				if target_tempo >= 20.0 and target_tempo <= 999.0:
					self._tempo_ramp_settings = [target_tempo, (target_tempo - self.song().tempo) / ramp_factor]
					self._tempo_ramp_active = True
			except: pass
		
	def apply_tempo_ramp(self, arg=None):
		""" Apply tempo smoothing """
		target_reached = False
		if self._tempo_ramp_settings[1] > 0:
			target_reached = self._tempo_ramp_settings[0] <= self.song().tempo
		else:
			target_reached = self._tempo_ramp_settings[0] >= self.song().tempo
		if target_reached:
			self.song().tempo = self._tempo_ramp_settings[0]
			self._tempo_ramp_active = False
			self._tasks.kill()
			self._tasks.clear()
		else:
			self.song().tempo += self._tempo_ramp_settings[1] 

