#!/bin/bash

echo "Killing Live."
killall Live
echo "Deploying scripts."
cp -R SandR_GangsterOHM /Applications/Ableton\ Live\ 9\ Suite.app/Contents/App-Resources/MIDI\ Remote\ Scripts/
open -a Ableton\ Live\ 9\ Suite
echo "Launching Live."
tail -n0 -f ~/Library/Preferences/Ableton/Live\ 9.1.8/Log.txt | tee >( grep -qx "Exit: End" )
exit